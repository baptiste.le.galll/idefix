export function checkBody(properties: any, req: any): any {
  const emptyProp = properties.filter(
    (prop: any) => !req.body.hasOwnProperty(prop)
  );

  return {
    valid: emptyProp.length === 0,
    empty: emptyProp,
  };
}

export function sendMissingProperties(
  check: any,
  currentRoute: any,
  req: any,
  res: any
): any {
  res.status(400).json({
    status: {
      success: 0,
      route: req.method + " : " + currentRoute + req.path,
    },
    error: {
      status: 400,
      message: "There are missing properties in the request.",
      propertiesMissing: check.empty,
    },
    params: req.body,
  });
}

export function sendSuccess(data: any, currentRoute: any, req: any, res: any): any {
  console.log({
    status: {
      success: 1,
      route: req.method + " : " + currentRoute + req.path,
    },
    params: req.params,
    body: req.body,
    token: req.headers.authorization,
    data: data,
  });
  res.status(200).json({
    status: {
      success: 1,
      route: req.method + " : " + currentRoute + req.path,
    },
    data: data,
  });
}

export function sendError(err: any, currentRoute: any, req: any, res: any): any {
  console.log({
    status: {
      success: 0,
      route: req.method + " : " + currentRoute + req.path,
    },
    params: req.params,
    body: req.body,
    token: req.headers.authorization,
    error: {
      status: err.status,
      message: err.message,
      error: err,
    },
  });
  res.status(404).json({
    status: {
      success: 0,
      route: req.method + " : " + currentRoute + req.path,
    },
    error: {
      status: err.status,
      message: err.message,
      error: err,
    },
  });
}

module.exports = {
  checkBody,
  sendMissingProperties,
  sendSuccess,
  sendError,
};
