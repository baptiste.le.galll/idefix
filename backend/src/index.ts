import dotenv from "dotenv";
import express from "express";

import { lessonRouter } from "./routes/lessonRoutes";
import { voteRouter } from "./routes/voteRoutes";
import { userRouter } from "./routes/userRoutes";
import { groupRouter} from "./routes/groupRoutes";
// import { updateFileRouter } from "./routes/updateFileRoutes";
import { postRouter } from "./routes/postRoutes";
import { tpRouter } from "./routes/tpRoutes";

const bodyParser = require("body-parser");
const Umzug = require("umzug");

// initialize configuration
dotenv.config();

import sequelize from "./models/init-models";
import Auth from "./middlewares/auth";

import { idefluxRouter } from "./routes/idefluxRoutes";
import { documentRouter } from "./routes/documentRoute";
import { tagRouter } from "./routes/tagRoutes";
import { projectRouter } from "./routes/projectRoutes";
import { responseRouter } from "./routes/responseRoute";
// port is now available to the Node.js runtime
// as if it were an environment variable
const port = process.env.SERVER_PORT;

const app = express();

/** bodyParser.urlencoded(options)
 * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body
 */
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS');
    next();
});


//Create Umzug environment for migrations
const umzug = new Umzug({
  migrations: {
    pattern: /^\d+[\w-]+\.ts$/,
    glob: ["../migrations/*.ts"],
  },
  context: sequelize,
  logger: console,
});

// define a route handler for the default home page

app.use("/users", userRouter);
app.use("/user-group", groupRouter);
app.use("/posts", postRouter);
app.use("/votes", voteRouter);
app.use("/documents", documentRouter);
// app.use("/updateFile", updateFileRouter);
app.use("/projects", projectRouter);
app.use("/tags", tagRouter);
app.use("/lessons", lessonRouter)
app.use("/ideflux", idefluxRouter)
app.use("/tps", tpRouter);
app.use("/responses", responseRouter);

app.get("/", async (req, res) => {
  sequelize
    .authenticate()
    .then(() => {
      console.log("Connection has been established successfully.");
    })
    .catch((err) => {
      console.error("Unable to connect to the database:", err);
    });
  // render the index template
  res.send("Hello World !");
});

// start the express server
app.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log(`server started at http://localhost:${port}`);
});
