import { Table, Column, Model, DataType, PrimaryKey, AutoIncrement, Unique, BelongsTo, ForeignKey } from 'sequelize-typescript'
import Post from './post.model'
import Response from './response.model'
import User from './user.model'

@Table({
  timestamps: true
})
export default class Comment extends Model {

    @PrimaryKey
    @AutoIncrement
    @Unique
    @Column
    id : number
    
    @Column(DataType.TEXT)
    content: string

    @ForeignKey(() => Post)
    @Column
    post_id: number
    @BelongsTo(() => Post) post: Post;

    @ForeignKey(() => Response)
    @Column
    reponse_id: number
    @BelongsTo(() => Response) response: Response;

    @ForeignKey(() => User)
    @Column
    created_user_id: number
    @BelongsTo(() => User) user: User;
}