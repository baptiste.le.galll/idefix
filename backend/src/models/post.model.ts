import { Table, Column, Model, DataType, PrimaryKey, AutoIncrement, Unique, BelongsToMany, BelongsTo, ForeignKey, HasMany } from 'sequelize-typescript'
import LessonPost from './lesson-post.model'
import Lesson from './lesson.model'
import ProjectPost from './project-post.model'
import Project from './project.model'
import Response from './response.model'
import TpPost from './tp-post.model'
import Tp from './tp.model'
import User from './user.model'
import Vote from './vote.model'

@Table({
  timestamps: true
})
export default class Post extends Model {

    @PrimaryKey
    @AutoIncrement
    @Unique
    @Column
    id : number
    
    @Column(DataType.TEXT)
    title: string

    @Column(DataType.TEXT)
    content: string

    @Column(DataType.TEXT)
    git_link: string

    @ForeignKey(() => User)
    @Column
    created_user_id: number

    @BelongsTo(() => User) user: User;

    @BelongsToMany(() => Tp, () => TpPost)
    tps: Tp[]

    @BelongsToMany(() => Project, () => ProjectPost)
    projects: Project[]

    @BelongsToMany(() => Lesson, () => LessonPost)
    lessons: Lesson[]

    @HasMany(() => Vote)
    votes: Vote[]

    @HasMany(() => Response)
    responses: Response[]
}