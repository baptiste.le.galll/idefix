import {Sequelize} from 'sequelize-typescript';
import Comment from './comment.model';
import Group from './group.model';
import Lesson from './lesson.model';
import Post from './post.model';
import ProjectPost from './project-post.model';
import Project from './project.model';
import Response from './response.model';
import TagLesson from './tag-lesson.model';
import TagProject from './tag-project.model';
import TagUser from './tag-user.model';
import Tag from './tag.model';
import TpPost from './tp-post.model';
import Tp from './tp.model';
import UserGroup from './user-group.model';
import User from './user.model';
import Vote from './vote.model';
import Document from './document.model';
import Ideflux from './ideflux.model';
import TagIdeflux from './tag-ideflux.model';
import LessonPost from './lesson-post.model';

const sequelize = new Sequelize(
  process.env.DBNAME,
  process.env.DBUSERNAME,
  process.env.DBPASSWORD,
  {
    host: process.env.DBURI,
    //language of database
    dialect: "postgres",
    port: +process.env.PORT,
    sync: { force: true},
    logging: console.log
  }
);

sequelize.addModels([Group, User, UserGroup, Tag, TagUser, Post, Response, Vote, Comment, Lesson, Tp, TpPost, Project, ProjectPost, TagProject, TagLesson, Document, Ideflux, TagIdeflux, LessonPost]);
// sequelize.sync()
// .then(
//   (res) => {
//     console.log("Syncronisation OK !")
//   }
// ).catch(
//   (err) => console.log("sync : err", err)
// )

export default sequelize