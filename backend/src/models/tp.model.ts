import { Table, Column, Model, DataType, PrimaryKey, AutoIncrement, Unique, BelongsToMany, ForeignKey, BelongsTo } from 'sequelize-typescript'
import Group from './group.model'
import Lesson from './lesson.model'
import Post from './post.model'
import TagUser from './tag-user.model'
import TpPost from './tp-post.model'
import User from './user.model'

@Table({
  timestamps: true
})
export default class Tp extends Model {

    @PrimaryKey
    @AutoIncrement
    @Unique
    @Column
    id : number
    
    @Unique
    @Column(DataType.TEXT)
    title: string

    @Column(DataType.TEXT)
    content: string

    @ForeignKey(() => Lesson)
    @Column
    lesson_id: number
    @BelongsTo(() => Lesson) lesson : Lesson;

    @ForeignKey(() => Group)
    @Column
    group_id: number
    @BelongsTo(() => Group) group: Group;

    @ForeignKey(() => User)
    @Column
    created_user_id: number
    @BelongsTo(() => User) user: User;

    @BelongsToMany(() => Post, () => TpPost)
    posts: Post[]
}