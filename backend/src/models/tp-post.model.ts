import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript'
import Post from './post.model'
import Tp from './tp.model'

@Table({
  timestamps: true
})
export default class TpPost extends Model {
  
  @Column
  @ForeignKey(() => Post)
  post_id: number

  @Column
  @ForeignKey(() => Tp)
  tp_id: number

  @BelongsTo(() => Post)
  post : Post

  @BelongsTo(() => Tp)
  tp : Tp
}