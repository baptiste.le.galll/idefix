import { Table, Column, Model, DataType, PrimaryKey, AutoIncrement, Unique, BelongsTo, ForeignKey, HasMany } from 'sequelize-typescript'
import Post from './post.model'
import User from './user.model'
import Vote from './vote.model'

@Table({
  timestamps: true
})
export default class Response extends Model {

    @PrimaryKey
    @AutoIncrement
    @Unique
    @Column
    id : number
    
    @Column(DataType.TEXT)
    content: string

    @ForeignKey(() => Post)
    @Column
    post_id: number

    @BelongsTo(() => Post) post: Post;

    @ForeignKey(() => User)
    @Column
    created_user_id: number

    @BelongsTo(() => User) user: User;

    @HasMany(() => Vote)
    votes: Vote[]
}