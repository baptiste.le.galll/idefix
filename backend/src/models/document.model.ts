import { Table, Column, Model, DataType, PrimaryKey, AutoIncrement, Unique, BelongsTo, ForeignKey } from 'sequelize-typescript'
import Lesson from './lesson.model'
import Project from './project.model'
import User from './user.model'

@Table({
  timestamps: true
})
export default class Document extends Model {

    @PrimaryKey
    @AutoIncrement
    @Unique
    @Column
    id : number
    
    @Column(DataType.TEXT)
    title: string

    @Column(DataType.INTEGER)
    type: number

    @ForeignKey(() => Lesson)
    @Column
    lesson_id: number
    @BelongsTo(() => Lesson) lesson: Lesson;

    @ForeignKey(() => Project)
    @Column
    project_id: number
    @BelongsTo(() => Project) project: Project;

    @ForeignKey(() => User)
    @Column
    created_user_id: number
    @BelongsTo(() => User) user: User;
}