import { Table, Column, Model, DataType, PrimaryKey, AutoIncrement, Unique, BelongsToMany } from 'sequelize-typescript'
import Group from './group.model'
import TagUser from './tag-user.model'
import Tag from './tag.model'
import UserGroup from './user-group.model'

@Table({
  timestamps: true
})
export default class User extends Model {

  @PrimaryKey
  @AutoIncrement
  @Unique
  @Column
  id : number
  
  @Unique
  @Column(DataType.TEXT)
  pseudo: string

  @Unique
  @Column(DataType.TEXT)
  email: string

  @Column(DataType.TEXT)
  password: string

  @BelongsToMany(() => Group, () => UserGroup)
  groups: Group[]

  @BelongsToMany(() => Tag, () => TagUser)
  tags: Tag[]
}