import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript'
import Tag from './tag.model'
import Project from './project.model'

@Table({
  timestamps: true
})
export default class TagProject extends Model {
  
  @Column
  @ForeignKey(() => Tag)
  tag_id: number

  @Column
  @ForeignKey(() => Project)
  project_id: number

  @BelongsTo(() => Tag)
  tag : Tag

  @BelongsTo(() => Project)
  project : Project
}