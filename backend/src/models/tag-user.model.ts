import { Table, Column, Model, DataType, ForeignKey, BelongsTo } from 'sequelize-typescript'
import User from './user.model'
import Tag from './tag.model'

@Table({
  timestamps: true
})
export default class TagUser extends Model {
  
  @Column
  @ForeignKey(() => User)
  user_id: number

  @Column
  @ForeignKey(() => Tag)
  tag_id: number

  @BelongsTo(() => User)
  user : User

  @BelongsTo(() => Tag)
  tag : Tag
}