import { Table, Column, Model, DataType, PrimaryKey, AutoIncrement, Unique, BelongsTo, ForeignKey, BelongsToMany } from 'sequelize-typescript'
import Post from './post.model'
import TagIdeflux from './tag-ideflux.model';
import Tag from './tag.model';


@Table({
  timestamps: true
})
export default class Ideflux extends Model {

    @PrimaryKey
    @AutoIncrement
    @Unique
    @Column
    id : number
    
    @ForeignKey(() => Post)
    @Column
    post_id: number
    @BelongsTo(() => Post) post: Post;

    @BelongsToMany(() => Tag, () => TagIdeflux)
    tags: Tag[]
}