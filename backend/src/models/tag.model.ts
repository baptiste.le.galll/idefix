import { Table, Column, Model, DataType, PrimaryKey, AutoIncrement, Unique, BelongsToMany } from 'sequelize-typescript'
import Lesson from './lesson.model'
import Project from './project.model'
import TagLesson from './tag-lesson.model'
import TagProject from './tag-project.model'
import TagUser from './tag-user.model'
import User from './user.model'

@Table({
  timestamps: true
})
export default class Tag extends Model {

    @PrimaryKey
    @AutoIncrement
    @Unique
    @Column
    id : number
    
    @Unique
    @Column(DataType.TEXT)
    title: string

    @Column(DataType.TEXT)
    description: string

    @BelongsToMany(() => User, () => TagUser)
    users: User[]

    @BelongsToMany(() => Project, () => TagProject)
    projects: Project[]

    @BelongsToMany(() => Lesson, () => TagLesson)
    lessons: Lesson[]
}