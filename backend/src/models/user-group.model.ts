import { Table, Column, Model, DataType, ForeignKey, BelongsTo } from 'sequelize-typescript'
import User from './user.model'
import Group from './group.model'

@Table({
  timestamps: true
})
export default class UserGroup extends Model {
  
  @Column
  @ForeignKey(() => User)
  user_id: number

  @Column
  @ForeignKey(() => Group)
  group_id: number

  @BelongsTo(() => User)
  user : User

  @BelongsTo(() => Group)
  group : Group
}