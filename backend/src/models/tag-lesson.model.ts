import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript'
import Tag from './tag.model'
import Lesson from './lesson.model'

@Table({
  timestamps: true
})
export default class TagLesson extends Model {
  
  @Column
  @ForeignKey(() => Tag)
  tag_id: number

  @Column
  @ForeignKey(() => Lesson)
  lesson_id: number

  @BelongsTo(() => Tag)
  tag : Tag

  @BelongsTo(() => Lesson)
  lesson : Lesson
}