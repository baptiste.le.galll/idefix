import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript'
import Post from './post.model'
import Project from './project.model'
import Tp from './tp.model'

@Table({
  timestamps: true
})
export default class ProjectPost extends Model {
  
  @Column
  @ForeignKey(() => Post)
  post_id: number

  @Column
  @ForeignKey(() => Project)
  project_id: number

  @BelongsTo(() => Post)
  post : Post

  @BelongsTo(() => Project)
  project : Tp
}