import { Table, Column, Model, DataType, PrimaryKey, AutoIncrement, Unique, BelongsTo, ForeignKey, BelongsToMany } from 'sequelize-typescript'
import Group from './group.model'
import LessonPost from './lesson-post.model'
import Post from './post.model'
import TagLesson from './tag-lesson.model'
import Tag from './tag.model'
import User from './user.model'

@Table({
  timestamps: true
})
export default class Lesson extends Model {

    @PrimaryKey
    @AutoIncrement
    @Unique
    @Column
    id : number
    
    @Column(DataType.TEXT)
    title: string

    @Column(DataType.TEXT)
    content: string

    @ForeignKey(() => Group)
    @Column
    group_id: number
    @BelongsTo(() => Group) group: Group;


    @ForeignKey(() => User)
    @Column
    created_user_id: number
    @BelongsTo(() => User) user: User;

    @BelongsToMany(() => Tag, () => TagLesson)
    tags: Tag[]

    @BelongsToMany(() => Post, () => LessonPost)
    posts: Post[]
}