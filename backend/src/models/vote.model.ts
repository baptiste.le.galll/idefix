import { Table, Column, Model, DataType, PrimaryKey, AutoIncrement, Unique, BelongsTo, ForeignKey } from 'sequelize-typescript'
import Post from './post.model'
import Response from './response.model'
import User from './user.model'

@Table({
  timestamps: true
})
export default class Vote extends Model {

    @PrimaryKey
    @AutoIncrement
    @Unique
    @Column
    id : number
    
    @Column(DataType.BOOLEAN)
    is_up: boolean

    @ForeignKey(() => Post)
    @Column
    post_id: number
    @BelongsTo(() => Post) post: Post;

    @ForeignKey(() => Response)
    @Column
    response_id: number
    @BelongsTo(() => Response) response: Post;

    @ForeignKey(() => User)
    @Column
    created_user_id: number
    @BelongsTo(() => User) user: User;
}