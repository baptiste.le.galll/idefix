import { Table, Column, Model, DataType, PrimaryKey, AutoIncrement, Unique, BelongsToMany, ForeignKey, BelongsTo } from 'sequelize-typescript'
import Group from './group.model'
import Post from './post.model'
import ProjectPost from './project-post.model'
import TagProject from './tag-project.model'
import Tag from './tag.model'
import User from './user.model'

@Table({
  timestamps: true
})
export default class Project extends Model {

    @PrimaryKey
    @AutoIncrement
    @Unique
    @Column
    id : number
    
    @Unique
    @Column(DataType.TEXT)
    title: string

    @Column(DataType.TEXT)
    content: string

    @Column(DataType.TEXT)
    git_link : string

    @Column(DataType.TEXT)
    access_token : string

    @Column(DataType.TEXT)
    path_folder : string

    @ForeignKey(() => Group)
    @Column
    group_id: number
    @BelongsTo(() => Group) group: Group;

    @ForeignKey(() => User)
    @Column
    created_user_id: number
    @BelongsTo(() => User) user: User;

    @BelongsToMany(() => Post, () => ProjectPost)
    posts: Post[]

    @BelongsToMany(() => Tag, () => TagProject)
    tags: Tag[]
}