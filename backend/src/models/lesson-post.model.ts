import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript'
import Lesson from './lesson.model'
import Post from './post.model'

@Table({
  timestamps: true
})
export default class LessonPost extends Model {
  
  @Column
  @ForeignKey(() => Post)
  post_id: number

  @Column
  @ForeignKey(() => Lesson)
  lesson_id: number

  @BelongsTo(() => Post)
  post : Post

  @BelongsTo(() => Lesson)
  lesson : Lesson
}