import { Table, Column, Model, DataType, PrimaryKey, AutoIncrement, Unique, BelongsToMany } from 'sequelize-typescript'
import User from './user.model'
import UserGroup from './user-group.model'

@Table({
  timestamps: true
})
export default class Group extends Model {

  @PrimaryKey
  @AutoIncrement
  @Unique
  @Column
  id : number
  
  @Unique
  @Column(DataType.TEXT)
  title: string

  @BelongsToMany(() => User, () => UserGroup)
  users: User[]
}