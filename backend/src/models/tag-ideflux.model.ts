import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript'
import Tag from './tag.model'
import Ideflux from './ideflux.model'

@Table({
  timestamps: true
})
export default class TagIdeflux extends Model {
  
  @Column
  @ForeignKey(() => Tag)
  tag_id: number

  @Column
  @ForeignKey(() => Ideflux)
  ideflux_id: number

  @BelongsTo(() => Tag)
  tag : Tag

  @BelongsTo(() => Ideflux)
  ideflux : Ideflux
}