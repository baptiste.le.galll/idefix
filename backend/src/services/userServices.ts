import User from "../models/user.model";

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

export default class UserService {
    constructor() {}

    static async createUser(email : string, pseudo : string, password: string) : Promise<User> {

        const hash = await bcrypt.hash(password,10);

        const user :User = await User.create({
            pseudo: pseudo,
            email: email,
            password: hash,
            score: 0,
        })

        return user
    }

    static async updateUser(userId : number, email : string, pseudo : string, password: string) : Promise<User> {

        const hash = await bcrypt.hash(password,10);

        const user :User = await (await User.findByPk(userId)).update({
            pseudo: pseudo,
            email: email,
            password: hash,
        })

        return user
    }

    static async login(email : string, hash_password : string) : Promise<User> {
        return User.findOne({
            where : {email : email},
            include:[
                {association : "tags", through: { attributes: [] }},
                {association : "groups", through: { attributes: [] }}
            ]
        }).then(
            (user : User) => {
                if (user) {
                    return bcrypt.compare(hash_password,user.password)
                    .then(
                        (result : boolean) => {
                            if (result) {
                                return Promise.resolve({
                                    token: jwt.sign(
                                        { userId: user.id },
                                        process.env.RANDOM_TOKEN_SECRET
                                    ),
                                    user : user
                                })
                            }
                                
                            else 
                                return Promise.reject(new Error("Wrong password"))
                        }
                    )
                } else {
                    return Promise.reject(new Error("Wrong email"))
                }                
            }  
        )
    }
}