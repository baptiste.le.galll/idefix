
const jwt = require('jsonwebtoken');

export default class Auth {

    static auth(req, res, next) {
        try {
            const token        = req.headers.authorization
            const decodedToken = jwt.verify(token, process.env.RANDOM_TOKEN_SECRET);
            const userId       = decodedToken.userId;

            if (req.body.userId && req.body.userId !== userId) {
            throw 'Invalid user ID';
            } else {
            next();
            }
        } catch {
            res.status(401).json({
            error: 'Invalid request! Token Error !'
            });
        }
    }

}
