import express from "express";

import { checkBody, sendMissingProperties, sendSuccess, sendError } from "../utils/utils";

import Document from "../models/document.model";

export const documentRouter = express.Router();

const currentRoute = "documents";

documentRouter.get("/", async (req: any, res: any) => {
  Document.findAll()
    .then((result: any) => sendSuccess(result, currentRoute, req, res))
    .catch((err: any) => sendError(err, currentRoute, req, res));
});


documentRouter.post("/", async (req, res) => {
    const propertiesMendatories = ["title", "type", "lesson_id", "project_id", "created_user_id"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
      sendMissingProperties(check, currentRoute, req, res);
    } else {
      Document.create({
        title: req.body.title,
        type: req.body.type,
        lesson_id: req.body.lesson_id,
        project_id: req.body.project_id,
        created_user_id: req.body.created_user_id
      })
        .then((result) => sendSuccess(result, currentRoute, req, res))
        .catch((err) => sendError(err, currentRoute, req, res));
    }
  });

  documentRouter.get("/:id", async (req: any, res: any) => {
    let documentId = parseInt(req.params.id)
     Document.findByPk(documentId)
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   });


   documentRouter.get("/", async (req: any, res: any) => {
    Document.findAll()
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   });

   documentRouter.post("/delete/:id", async (req:any, res:any) => {
    let documentId = req.params.id;
    Document.destroy({where: {id:documentId}})
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   })

   documentRouter.post("/update/:id", async (req:any, res:any) => {
    let documentId = req.params.id;
    const propertiesMendatories = ["title", "type", "lesson_id", "project_id", "created_user_id"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
      sendMissingProperties(check, currentRoute, req, res);
    } else {
       (await Document.findByPk(documentId)).update({
          title: req.body.title,
          type: req.body.type,
          lesson_id: req.body.lesson_id,
          project_id: req.body.project_id,
          created_user_id: req.body.created_user_id
        })
        .then((result) => sendSuccess(result, currentRoute, req, res))
        .catch((err) => sendError(err, currentRoute, req, res));
    }
   })