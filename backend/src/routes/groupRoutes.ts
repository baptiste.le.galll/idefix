import express from "express";
import { checkBody, sendMissingProperties, sendSuccess, sendError } from "../utils/utils";

import Group from "../models/group.model";

export const groupRouter = express.Router();

const currentRoute = "group";

groupRouter.get("/", async (req, res) => {
  Group.findAll()
    .then((result) => sendSuccess(result, currentRoute, req, res))
    .catch((err) => sendError(err, currentRoute, req, res));
});


groupRouter.post("/", async (req, res) => {
  const propertiesMendatories = ["title"];
  const check = checkBody(propertiesMendatories, req);
  if (!check.valid) {
    sendMissingProperties(check, currentRoute, req, res);
  } else {
    Group.create({
      title: req.body.title
    })
    .then((result) => sendSuccess(result, currentRoute, req, res))
    .catch((err) => sendError(err, currentRoute, req, res));
  }
});


groupRouter.post("/update/:id", async (req:any, res:any) => {
  let tagId = req.params.id;
  const propertiesMendatories = ["title"];
  const check = checkBody(propertiesMendatories, req);
  if (!check.valid) {
    sendMissingProperties(check, currentRoute, req, res);
  } else {
    (await Group.findByPk(tagId)).update({
        title: req.body.title
    })
    .then((result) => sendSuccess(result, currentRoute, req, res))
    .catch((err) => sendError(err, currentRoute, req, res));
  }
})


groupRouter.post("/delete/:id", async (req:any, res:any) => {
  let tagId = req.params.id;
  Group.destroy({where: {id:tagId}})
  .then((result: any) => sendSuccess(result, currentRoute, req, res))
  .catch((err: any) => sendError(err, currentRoute, req, res));
})