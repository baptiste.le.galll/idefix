import express from "express";

import { checkBody, sendMissingProperties, sendSuccess, sendError } from "../utils/utils";

import { NO_ID } from "../utils/constants";
import Response from "../models/response.model";

export const responseRouter = express.Router();

const currentRoute = "responses";

responseRouter.get("/", async (req: any, res: any) => {
  Response.findAll()
    .then((result: any) => sendSuccess(result, currentRoute, req, res))
    .catch((err: any) => sendError(err, currentRoute, req, res));
});


responseRouter.post("/", async (req, res) => {
    const propertiesMendatories = ["content", "post_id", "created_user_id"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
      sendMissingProperties(check, currentRoute, req, res);
    } 
        Response.create({
          content: req.body.content,
          post_id: req.body.post_id,
          created_user_id: req.body.created_user_id
        })
        .then((result) => sendSuccess(result, currentRoute, req, res))
        .catch((err) => sendError(err, currentRoute, req, res));
    
  });

  responseRouter.get("/:id", async (req: any, res: any) => {
    let responseId = parseInt(req.params.id)
     Response.findByPk(responseId)
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   });

   responseRouter.post("/delete/:id", async (req:any, res:any) => {
    let responseId = req.params.id;
    Response.destroy({where: {id:responseId}})
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   })

   responseRouter.post("/update/:id", async (req:any, res:any) => {
    let responseId = req.params.id;
    const propertiesMendatories = ["content", "post_id", "created_user_id"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
      sendMissingProperties(check, currentRoute, req, res);
    } else {
       (await Response.findByPk(responseId)).update({
            content: req.body.content,
            post_id: req.body.post_id,
            created_user_id: req.body.created_user_id
        })
        .then((result) => sendSuccess(result, currentRoute, req, res))
        .catch((err) => sendError(err, currentRoute, req, res));
    }
   })