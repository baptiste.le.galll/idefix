import express from "express";

import { checkBody, sendMissingProperties, sendSuccess, sendError } from "../utils/utils";

import Tag from "../models/tag.model";

export const tagRouter = express.Router();

const currentRoute = "tags";

tagRouter.get("/", async (req: any, res: any) => {
  Tag.findAll()
    .then((result: any) => sendSuccess(result, currentRoute, req, res))
    .catch((err: any) => sendError(err, currentRoute, req, res));
});


tagRouter.post("/", async (req, res) => {
  const propertiesMendatories = ["title", "description"];
  const check = checkBody(propertiesMendatories, req);
  if (!check.valid) {
    sendMissingProperties(check, currentRoute, req, res);
  } else {
    Tag.create({
      title: req.body.title,
      description: req.body.description,
    })
    .then((result) => sendSuccess(result, currentRoute, req, res))
    .catch((err) => sendError(err, currentRoute, req, res));
  }
});

tagRouter.get("/:id", async (req: any, res: any) => {
  let postId = parseInt(req.params.id)
  Tag.findByPk(postId)
  .then((result: any) => sendSuccess(result, currentRoute, req, res))
  .catch((err: any) => sendError(err, currentRoute, req, res));
});


tagRouter.post("/delete/:id", async (req:any, res:any) => {
  let tagId = req.params.id;
  Tag.destroy({where: {id:tagId}})
  .then((result: any) => sendSuccess(result, currentRoute, req, res))
  .catch((err: any) => sendError(err, currentRoute, req, res));
})

tagRouter.post("/update/:id", async (req:any, res:any) => {
  let tagId = req.params.id;
  const propertiesMendatories = ["title", "description"];
  const check = checkBody(propertiesMendatories, req);
  if (!check.valid) {
    sendMissingProperties(check, currentRoute, req, res);
  } else {
      (await Tag.findByPk(tagId)).update({
          title: req.body.title,
          description: req.body.description,
      })
      .then((result) => sendSuccess(result, currentRoute, req, res))
      .catch((err) => sendError(err, currentRoute, req, res));
  }
})