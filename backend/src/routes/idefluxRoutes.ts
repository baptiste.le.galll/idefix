import express from "express";
import Tag from "../models/tag.model";
import Ideflux from "../models/ideflux.model";

import { checkBody, sendMissingProperties, sendSuccess, sendError } from "../utils/utils";
import Post from "../models/post.model";
import Vote from "../models/vote.model";
import Response from "../models/response.model";
import { where } from "sequelize";

export const idefluxRouter = express.Router();

const currentRoute = "ideflux";

idefluxRouter.get("/", async (req: any, res: any) => {
 await Ideflux.findAll({
   include : [
    {
      association : "tags", through: {attributes : []}
    },
    {
      model: Post,
      required: true,
      include : [
        {
          model: Vote,
        },
        {
          model : Response
        }
      ]
    }
  ]
 })
  .then((result: any) => sendSuccess(result, currentRoute, req, res))
  .catch((err: any) => sendError(err, currentRoute, req, res));
});

idefluxRouter.post("/", async (req, res) => {
    const propertiesMendatories = ["post_id, tags"];
    //const check = checkBody(propertiesMendatories, req);

    //if (!check.valid) {
      //sendMissingProperties(check, currentRoute, req, res);
    //} else {
      const ideflux = await Ideflux.create({
        post_id: req.body.post_id,
      });
      
      await req.body.tags.map(async tagId => {
        let tag = await Tag.findOne({
          where : {
            id : tagId
          }
        })
        ideflux.$add("tags", tag)
      })

      Ideflux.findOne({
        where: {
          id: ideflux.id
        },
        include : [
          {
            association: "tags",
            through: {
              attributes: []
            }
          },
          {
            model: Post,
            required: true,
            include : [
              {
                model: Vote,
              },
              {
                model : Response,
                include: [{
                  model: Vote
                }]
              }
            ]
          }
        ]
      })
      .then((result: any) => sendSuccess(result, currentRoute, req, res))
      .catch((err: any) => sendError(err, currentRoute, req, res));
    
  //}
});

idefluxRouter.get("/:id", async (req: any, res: any) => {
  let idefluxId = parseInt(req.params.id)
  await Ideflux.findOne({
    where: {
      id: idefluxId
    },
    include : [
      {
        association: "tags",
        through: {
          attributes: []
        }
      },
      {
        model: Post,
        required: true,
        include : [
          {
            model: Vote,
          },
          {
            model : Response,
            include: [{
              model: Vote
            }]
          }
        ]
      }
    ]
  })
  .then((result: any) => sendSuccess(result, currentRoute, req, res))
  .catch((err: any) => sendError(err, currentRoute, req, res));
});


idefluxRouter.post("/delete/:id", async (req:any, res:any) => {
  let idefluxId = req.params.id;
  await Ideflux.destroy({where: {id:idefluxId}})
      .then((result: any) => sendSuccess(result, currentRoute, req, res))
      .catch((err: any) => sendError(err, currentRoute, req, res));
})

idefluxRouter.get("/user/:userId", async (req: any, res: any) => {
  let userId = req.params.userId;
  await Ideflux.findAll({
    include : [
    {
      association : "tags", through: {attributes : []}
    },
    {
      model: Post,
      required: true,
      where: {
        created_user_id: userId
      },
      include : [
        {
          model: Vote,
        },
        {
          model : Response
        }
      ]
    }
  ]
  })
  .then((result: any) => sendSuccess(result, currentRoute, req, res))
  .catch((err: any) => sendError(err, currentRoute, req, res));
})


idefluxRouter.post("/update/:id", async (req:any, res:any) => {
  let idefluxId = req.params.id;
  let newIdeflux = req.body.ideflux
  const propertiesMendatories = [];
  const check = checkBody(propertiesMendatories, req);

  if (!check.valid) {
    sendMissingProperties(check, currentRoute, req, res);
  } else {
    let ideflux = await Ideflux.findByPk(idefluxId,{
        include : [
        {
          association: "tags",
          through: {
            attributes: []
          }
        },
        {
          model: Post,
          required: true,
          include : [
            {
              model: Vote,
            },
            {
              model : Response,
              include: [{
                model: Vote
              }]
            }
          ]
        }
      ]
    })

    const removeTags = await ideflux.$remove("tags", ideflux.tags)

    const updatePostIdeflux = await (ideflux.post.update({
        title: newIdeflux.post.title,
        content: newIdeflux.post.content,
        git_link: newIdeflux.post.git_link
      })
    )

    const addTags = await ideflux.$add("tags", newIdeflux.tags)

    Ideflux.findOne({
      where: {
        id: idefluxId
      },
      include : [
        {
          association: "tags",
          through: {
            attributes: []
          }
        },
        {
          model: Post,
          required: true,
          include : [
            {
              model: Vote,
            },
            {
              model : Response,
              include: [{
                model: Vote
              }]
            }
          ]
        }
      ]
    })
    .then((result: any) => sendSuccess(result, currentRoute, req, res))
    .catch((err: any) => sendError(err, currentRoute, req, res));
    }
  })
