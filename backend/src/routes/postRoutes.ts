import express from "express";
import Vote from "../models/vote.model";
import Response from "../models/response.model";
import Post from "../models/post.model";

import { checkBody, sendMissingProperties, sendSuccess, sendError } from "../utils/utils";


export const postRouter = express.Router();

const currentRoute = "posts";

postRouter.get("/", async (req: any, res: any) => {
  await Post.findAll({
    include : [
            {
              model: Vote,
            },
            {
              model : Response
            }
          ]
  })
  .then((result: any) => sendSuccess(result, currentRoute, req, res))
  .catch((err: any) => sendError(err, currentRoute, req, res));
});

postRouter.post("/", async (req, res) => {
    const propertiesMendatories = ["title", "content", "git_link", "created_user_id"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
      sendMissingProperties(check, currentRoute, req, res);
    } else {
      await Post.create({
        title: req.body.title,
        content: req.body.content,
        git_link: req.body.git_link,
        created_user_id: req.body.created_user_id,
      })
        .then((result) => sendSuccess(result, currentRoute, req, res))
        .catch((err) => sendError(err, currentRoute, req, res));
    }
  });

  postRouter.get("/:id", async (req: any, res: any) => {
    let postId = parseInt(req.params.id)
    Post.findByPk(
      postId, {
        include : [
          {
            model: Vote,
          },
          {
            model : Response
          }
        ]
      })
      .then((result: any) => sendSuccess(result, currentRoute, req, res))
      .catch((err: any) => sendError(err, currentRoute, req, res));
   });


  postRouter.get("/", async (req: any, res: any) => {
    await Post.findAll()
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   });

   postRouter.post("/delete/:id", async (req:any, res:any) => {
       let postId = req.params.id;
       await Post.destroy({where: {id:postId}})
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   })

   postRouter.post("/update/:id", async (req:any, res:any) => {
    let postId = req.params.id;
    const propertiesMendatories = ["title", "content", "git_link"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
      sendMissingProperties(check, currentRoute, req, res);
    } else {
      await (await Post.findByPk(postId)).update({
            title: req.body.title,
            content: req.body.content,
            git_link: req.body.git_link
        })
        .then((result) => sendSuccess(result, currentRoute, req, res))
        .catch((err) => sendError(err, currentRoute, req, res));
    }
   })