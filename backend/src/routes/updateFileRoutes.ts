import express from "express";
import { sequelize } from "../umzug";
import { checkBody, sendMissingProperties, sendSuccess, sendError } from "../utils/utils";
const Umzug = require("umzug");

export const updateFileRouter = express.Router();

const currentRoute = "updateFile";
const umzug = new Umzug({
  migrations: {
    pattern: /^\d+[\w-]+\.ts$/,
    glob: ["../../migrations/*.ts"],
  },
  context: sequelize,
  logger: console,
});
updateFileRouter.get("/wait", async (req, res) => {
  const migrations = await umzug.pending();
  const numberOfMigration = Object.keys(migrations).length;
  let string = "";
  if (numberOfMigration > 0) {
    string = "Vous avez " + numberOfMigration + " fichier en attente";
  } else {
    string = "Vous êtes à jour dans vos scripts";
  }
  // render the index template

  sendSuccess(string, currentRoute, req, res);
});

updateFileRouter.get("/exec", async (req, res) => {
  const migrations = await umzug.up();
  let string = "Mise à jour OK";
  // render the index template
  sendSuccess(string, currentRoute, req, res);
});
