import express from "express";

import { checkBody, sendMissingProperties, sendSuccess, sendError } from "../utils/utils";

import Vote from "../models/vote.model";
import { NO_ID } from "../utils/constants";

export const voteRouter = express.Router();

const currentRoute = "votes";

voteRouter.get("/", async (req: any, res: any) => {
  Vote.findAll()
    .then((result: any) => sendSuccess(result, currentRoute, req, res))
    .catch((err: any) => sendError(err, currentRoute, req, res));
});


voteRouter.post("/", async (req, res) => {
    const propertiesMendatories = ["is_up", "post_id", "response_id", "created_user_id"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
      sendMissingProperties(check, currentRoute, req, res);
    } else {
      if (req.body.response_id === NO_ID) {
          Vote.create({
            is_up: req.body.is_up,
            post_id: req.body.post_id,
            created_user_id: req.body.created_user_id
          })
          .then((result) => sendSuccess(result, currentRoute, req, res))
          .catch((err) => sendError(err, currentRoute, req, res));
      } else if (req.body.post_id === NO_ID) {
          Vote.create({
            is_up: req.body.is_up,
            response_id: req.body.response_id,
            created_user_id: req.body.created_user_id
          })
          .then((result) => sendSuccess(result, currentRoute, req, res))
          .catch((err) => sendError(err, currentRoute, req, res));
      } else {
        sendError(new Error("post_id or response_id must be set to NO_ID"), currentRoute, req, res)
      }
      
    }
  });

  voteRouter.get("/:id", async (req: any, res: any) => {
    let voteId = parseInt(req.params.id)
     Vote.findByPk(voteId)
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   });


   voteRouter.get("/", async (req: any, res: any) => {
    Vote.findAll()
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   });

   voteRouter.post("/delete/:id", async (req:any, res:any) => {
    let voteId = req.params.id;
    Vote.destroy({where: {id:voteId}})
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   })

   voteRouter.post("/update/:id", async (req:any, res:any) => {
    let voteId = req.params.id;
    const propertiesMendatories = ["is_up", "post_id", "response_id", "created_user_id"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
      sendMissingProperties(check, currentRoute, req, res);
    } else {
       (await Vote.findByPk(voteId)).update({
            is_up: req.body.is_up,
            post_id: req.body.post_id,
            response_id: req.body.response_id,
            created_user_id: req.body.created_user_id
        })
        .then((result) => sendSuccess(result, currentRoute, req, res))
        .catch((err) => sendError(err, currentRoute, req, res));
    }
   })