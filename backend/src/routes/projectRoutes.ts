import express from "express";

import { checkBody, sendMissingProperties, sendSuccess, sendError } from "../utils/utils";

import Project from "../models/project.model";
import ProjectPost from "../models/project-post.model";
import Post from "../models/post.model";
import Tag from "../models/tag.model";
import Vote from "../models/vote.model";
import Response from "../models/response.model";


export const projectRouter = express.Router();

const currentRoute = "projects";

projectRouter.get("/", async (req, res) => {
  Project.findAll({include : [
    {
      association : "tags", through: {attributes : []}
    },
    {
      model: Post,
      required: false,
      include : [
        {
          model: Vote,
        },
        {
          model : Response
        }
      ]
    }
  ]})
    .then((result) => sendSuccess(result, currentRoute, req, res))
    .catch((err) => sendError(err, currentRoute, req, res));
});

projectRouter.get("/:id", async (req,res) => {
    let projectId: number;
    
    projectId = parseInt(req.params.id)
    Project.findOne({ where : {id : projectId},include : [
      {
        association: "tags",
        through: {
          attributes: []
        }
      },
      {
        model: Post,
        required: false,
        include : [
          {
            model: Vote,
          },
          {
            model : Response
          }
        ]
      }]})
      .then((result) => sendSuccess(result, currentRoute, req, res))
      .catch((err) => sendError(err, currentRoute, req, res));
})

// projectRouter.get("/posts", async (req,res) => {

//   Project.findAll({include : [
//     {
//       association : "tags", through: {attributes : []}
//     // },
//     // {
//     //   model: Post,
//     //   required: true,
//     //   include : [
//     //     {
//     //       model: Vote,
//     //     },
//     //     {
//     //       model : Response
//     //     }
//     //   ]
//     }]
//   })
// });


projectRouter.post("/", async (req, res) => {
  const propertiesMendatories = ["title", "content", "git_link", "access_token", "path_folder", "user_group_id", "created_user_id","tags"];
  const check = checkBody(propertiesMendatories, req);
  let project: Project;
  if (!check.valid) {
    sendMissingProperties(check, currentRoute, req, res);
  } else {  
    project = await Project.create({
        title: req.body.title,
        content: req.body.content,
        git_link: req.body.git_link,
        access_token: req.body.access_token,
        path_folder: req.body.path_folder,
        user_group_id: req.body.user_group_id,
        created_user_id: req.body.created_user_id
    });

    await req.body.tags.map(async tagId => {
      let tag = await Tag.findOne({
        where : {
          id : tagId
        }
      })
      project.$add("tags", tag)
    })

    Project.findByPk(project.id, {
      include : [
      {
        association: "tags",
        through: {
          attributes: []
        }
      },
      {
        model: Post,
        required: false,
        include : [
          {
            model: Vote,
          },
          {
            model : Response
          }
        ]
      }
    ]})
    .then((result) => sendSuccess(result, currentRoute, req, res))
    .catch((err) => sendError(err, currentRoute, req, res));
  }
});

projectRouter.post("/addpost", async (req, res) => {
  let projectId: number;
  let postId: number; 
  let post: Post;
  let project: Project;

  projectId = parseInt(req.body.project_id);
  postId = parseInt(req.body.post_id);

  project = await Project.findOne({ where : {id : projectId},include : [{ association : "tags", through: {attributes : []}  }]})

  post = await Post.findOne({ where : {id : postId}})

  project.$add("posts", post)

  Post.findByPk(postId)
  .then((result) => sendSuccess(result, currentRoute, req, res))
  .catch((err) => sendError(err, currentRoute, req, res));
  

})


projectRouter.post("/update/:id", async (req, res) => {
    let projectId: number;
    let project: Project;

    projectId = parseInt(req.params.id)

    project = await Project.findOne({ where : {id : projectId},include : [{ association : "tags", through: {attributes : []}  }]})
    .then((result) => result)
    .catch((err) => sendError(err, currentRoute, req, res)); 

    const propertiesMendatories = ["title", "content", "git_link", "access_token", "path_folder"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
      sendMissingProperties(check, currentRoute, req, res);
    } else {  
      project.update({
          title: req.body.title,
          content: req.body.content,
          git_link: req.body.git_link,
          acces_token: req.body.access_token,
          path_folder: req.body.path_folder,
      })
        .then((result) => sendSuccess(Project, currentRoute, req, res))
        .catch((err) => sendError(err, currentRoute, req, res));
    }
  });

projectRouter.post("/delete/:id", async (req, res) => {
    let projectId: number;
    let project: Project;

    projectId = parseInt(req.params.id)
    project = await Project.findOne({ where : {id : projectId} })
      .then((result) => result)
      .catch((err) => sendError(err, currentRoute, req, res)); 
    project.destroy().then((result) => sendSuccess(Project, currentRoute, req, res));
});

projectRouter.get("/user/:userId", async (req, res) => {
  let userId = req.params.userId;
     await Project.findAll({
      where: {
        created_user_id: userId
      },
      include : [
        {
          association : "tags", through: {attributes : []}
        },
        {
          association : "posts", through: {attributes : []}
        }
      ]
     })
     .then((result: any) => sendSuccess(result, currentRoute, req, res))
     .catch((err: any) => sendError(err, currentRoute, req, res));
})