import express from "express";
import Lesson from "../models/lesson.model";

import { checkBody, sendMissingProperties, sendSuccess, sendError } from "../utils/utils";

export const lessonRouter = express.Router();

const currentRoute = "lessons";

lessonRouter.get("/", async (req: any, res: any) => {
 await Lesson.findAll()
    .then((result: any) => sendSuccess(result, currentRoute, req, res))
    .catch((err: any) => sendError(err, currentRoute, req, res));
});

lessonRouter.post("/", async (req, res) => {
    const propertiesMendatories = ["title", "content"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
      sendMissingProperties(check, currentRoute, req, res);
    } else {
      await Lesson.create({
        title: req.body.title,
        content: req.body.content
      })
        .then((result) => sendSuccess(result, currentRoute, req, res))
        .catch((err) => sendError(err, currentRoute, req, res));
    }
  });

  lessonRouter.get("/:id", async (req: any, res: any) => {
    let lessonId = parseInt(req.params.id)
    await Lesson.findByPk(lessonId)
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   });


   lessonRouter.post("/delete/:id", async (req:any, res:any) => {
       let lessonId = req.params.id;
       await Lesson.destroy({where: {id:lessonId}})
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   })

   lessonRouter.post("/update/:id", async (req:any, res:any) => {
    let lessonId = req.params.id;
    const propertiesMendatories = ["title", "content"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
      sendMissingProperties(check, currentRoute, req, res);
    } else {
      await (await Lesson.findByPk(lessonId)).update({
            title: req.body.title,
            content: req.body.content
        })
        .then((result) => sendSuccess(result, currentRoute, req, res))
        .catch((err) => sendError(err, currentRoute, req, res));
    }
   })