import express from "express";
import Tp from "../models/tp.model";
import { checkBody, sendMissingProperties, sendSuccess, sendError } from "../utils/utils";

export const tpRouter = express.Router();

const currentRoute = "tps";

tpRouter.get("/", async (req: any, res: any) => {
    Tp.findAll()
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   });

tpRouter.get("/:id", async (req: any, res: any) => {
    let tpId: number;
  
    tpId = parseInt(req.params.id)
    Tp.findOne({ where : {id : tpId} })
       .then((result: any) => sendSuccess(result, currentRoute, req, res))
       .catch((err: any) => sendError(err, currentRoute, req, res));
   });

tpRouter.post("/", async (req, res) => {
    const propertiesMendatories = ["title", "content", "lesson_id", "group_id", "created_user_id"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
      sendMissingProperties(check, currentRoute, req, res);
    } else {
      await Tp.create({
        title: req.body.title,
        content: req.body.content,
        lesson_id: req.body.lesson_id,
        group_id: req.body.group_id,
        created_user_id: req.body.created_user_id
      })
        .then((result) => sendSuccess(result, currentRoute, req, res))
        .catch((err) => sendError(err, currentRoute, req, res));
    }
  });

tpRouter.post("/delete/:id", async (req:any, res:any) => {
    let tpId = req.params.id;
    await Tp.destroy({where: {id:tpId}})
    .then((result: any) => sendSuccess(result, currentRoute, req, res))
    .catch((err: any) => sendError(err, currentRoute, req, res));
})

tpRouter.post("/update/:id", async (req:any, res:any) => {
    let tpId = req.params.id;
    const propertiesMendatories = ["title", "content", "lesson_id", "group_id", "created_user_id"];
    const check = checkBody(propertiesMendatories, req);
    if (!check.valid) {
        sendMissingProperties(check, currentRoute, req, res);
    } else {
        await (await Tp.findByPk(tpId)).update({
            title: req.body.title,
            content: req.body.content,
            lesson_id: req.body.lesson_id,
            group_id: req.body.group_id,
            created_user_id: req.body.created_user_id
        })
        .then((result) => sendSuccess(result, currentRoute, req, res))
        .catch((err) => sendError(err, currentRoute, req, res));
    }
})