import express from "express";
import { checkBody, sendMissingProperties, sendSuccess, sendError } from "../utils/utils";

import User from "../models/user.model";

import UserService from "../services/userServices";
import Auth from "../middlewares/auth"
import Tag from "../models/tag.model";


export const userRouter = express.Router();

const currentRoute = "users";

userRouter.get("/", Auth.auth, async (req, res) => {
  User.findAll()
    .then((result) => sendSuccess(result, currentRoute, req, res))
    .catch((err) => sendError(err, currentRoute, req, res));
});

userRouter.get("/:id", async (req,res) => {
  let userId: number;
  
  userId = parseInt(req.params.id)
  User.findOne({
    where : {id : userId},
    include:[
      {association : "tags", through: { attributes: [] }},
      {association : "groups", through: { attributes: [] }}
    ]
  })
  .then((result) => sendSuccess(result, currentRoute, req, res))
  .catch((err) => sendError(err, currentRoute, req, res));
});

userRouter.post("/login", async(req, res) => {
  const propertiesMendatories = ["email", "password"];
  const check = checkBody(propertiesMendatories, req);

  if (!check.valid) {
    sendMissingProperties(check, currentRoute, req, res);
  } else {
    UserService.login(req.body.email, req.body.password)
    .then((result) => sendSuccess(result, currentRoute, req, res))
    .catch((err) => sendError(err, currentRoute, req, res));
  }
})

userRouter.post("/sign-in", async (req, res) => {
  const propertiesMendatories = ["pseudo", "email", "password"];
  const check = checkBody(propertiesMendatories, req);

  if (!check.valid) {
    sendMissingProperties(check, currentRoute, req, res);
  } else {
    UserService.createUser(req.body.email, req.body.pseudo, req.body.password)
    .then((userCreated : User) => sendSuccess(userCreated, currentRoute, req ,res))
    .catch((err) => sendError(err, currentRoute, req, res));
  }
});

userRouter.post("/update/:id", async (req:any, res:any) => {
  let userId = req.params.id;
  const propertiesMendatories = ["email", "pseudo", "password"];
  const check = checkBody(propertiesMendatories, req);
  if (!check.valid) {
    sendMissingProperties(check, currentRoute, req, res);
  } else {
    UserService.updateUser(userId, req.body.email, req.body.pseudo, req.body.password)
    .then((result) => sendSuccess(result, currentRoute, req, res))
    .catch((err) => sendError(err, currentRoute, req, res));
  }
});

userRouter.post("/tags", async (req: any, res: any) => {
  let tagId = req.body.tag_id;
  let userId = req.body.user_id;
  const user = await User.findByPk(userId);
  const tags = await Tag.findByPk(tagId);
  await user.$add("tags", tags)
  await Tag.findByPk(tagId)
  .then((result) => sendSuccess(result, currentRoute, req, res))
  .catch((err) => sendError(err, currentRoute, req, res));
})

userRouter.post("/tags/delete", async (req: any, res: any) => {
  let tagId = req.body.tag_id;
  let userId = req.body.user_id;
  const user = await User.findByPk(userId);
  const tags = await Tag.findByPk(tagId);
  await user.$remove("tags", tags)
  await Tag.findByPk(tagId)
  .then((result) => sendSuccess(result, currentRoute, req, res))
  .catch((err) => sendError(err, currentRoute, req, res));
})


userRouter.get("/tags/:userId", async (req:any, res:any) => {
  let userId = req.params.userId;
  const user = await User.findByPk(userId);
  user.$get("tags")
    .then((result) => sendSuccess(result, currentRoute, req, res))
    .catch((err) => sendError(err, currentRoute, req, res));
})


