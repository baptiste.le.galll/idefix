import { DataTypes, Sequelize } from "sequelize";

const sequelize = new Sequelize(
  process.env.DBNAME,
  process.env.DBUSERNAME,
  process.env.DBPASSWORD,
  {
    host: process.env.DBURI,
    //language of database
    dialect: "postgres",
    port: +process.env.PORT
  }
);
export const up = async (context: any) => {
    await sequelize.query("ALTER TABLE user_group ADD CONSTRAINT unique_user_group_title UNIQUE (title)")
    await sequelize.query("ALTER TABLE tag ADD CONSTRAINT unique_tag_title UNIQUE (title)")
};


