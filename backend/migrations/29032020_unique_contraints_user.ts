import { DataTypes, Sequelize } from "sequelize";

const sequelize = new Sequelize(
  process.env.DBNAME,
  process.env.DBUSERNAME,
  process.env.DBPASSWORD,
  {
    host: process.env.DBURI,
    //language of database
    dialect: "postgres",
    port: +process.env.PORT
  }
);
export const up = async (context: any) => {
  Promise.all([
    await sequelize.query("ALTER TABLE public.user ADD CONSTRAINT unique_user_email UNIQUE (email)"),
    await sequelize.query("ALTER TABLE public.user ADD CONSTRAINT unique_user_pseudo UNIQUE (pseudo)")
  ]).then(
    () => console.log("Tout à fonctionner")
  ).catch(
    (err) => console.log("Import BDD bug : ", err)
  )

};