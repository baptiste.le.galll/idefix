import { DataTypes, Sequelize } from "sequelize";

const sequelize = new Sequelize(
  process.env.DBNAME,
  process.env.DBUSERNAME,
  process.env.DBPASSWORD,
  {
    host: process.env.DBURI,
    //language of database
    dialect: "postgres",
  }
);
export const up = async (context: any) => {
  await sequelize.query("ALTER TABLE public.post ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
};
