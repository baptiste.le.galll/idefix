import { DataTypes, Sequelize } from "sequelize";

const sequelize = new Sequelize(
  process.env.DBNAME,
  process.env.DBUSERNAME,
  process.env.DBPASSWORD,
  {
    host: process.env.DBURI,
    //language of database
    dialect: "postgres",
  }
);
export const up = async (context: any) => {
  await sequelize.getQueryInterface().createTable("test", {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });
};

export const down = async (context: any) => {
  await sequelize.getQueryInterface().dropTable("test");
};
