import { DataTypes, Sequelize } from "sequelize";

const sequelize = new Sequelize(
  process.env.DBNAME,
  process.env.DBUSERNAME,
  process.env.DBPASSWORD,
  {
    host: process.env.DBURI,
    //language of database
    dialect: "postgres",
    port: +process.env.PORT
  }
);
export const up = async (context: any) => {
    await sequelize.query("ALTER TABLE public.commentaire ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.document ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.ideflux ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.lesson ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.lesson_post_rel ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.post ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.project ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.project_post_rel ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.reponse ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.tag ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.tag_ideflux_rel ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.tag_lesson_rel ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.tag_project_rel ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.tag_user_rel ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.tp ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.tp_post_rel ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
    await sequelize.query("ALTER TABLE public.vote ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 )")
};
