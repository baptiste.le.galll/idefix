--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: commentaire; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.commentaire (
    id integer NOT NULL,
    content text NOT NULL,
    created_user_id integer NOT NULL,
    reponse_id integer NOT NULL,
    post_id integer NOT NULL
);


ALTER TABLE public.commentaire OWNER TO postgres;

--
-- Name: document; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.document (
    id integer NOT NULL,
    title text NOT NULL,
    type integer NOT NULL,
    lesson_id integer NOT NULL,
    project_id integer NOT NULL,
    created_user_id integer NOT NULL
);


ALTER TABLE public.document OWNER TO postgres;

--
-- Name: ideflux; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ideflux (
    id integer NOT NULL,
    post_id integer NOT NULL
);


ALTER TABLE public.ideflux OWNER TO postgres;

--
-- Name: lesson; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lesson (
    id integer NOT NULL,
    title text NOT NULL,
    content text NOT NULL,
    user_group_id integer NOT NULL,
    created_user_id integer NOT NULL
);


ALTER TABLE public.lesson OWNER TO postgres;

--
-- Name: lessonPostRel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."lessonPostRel" (
    id integer NOT NULL,
    post_id integer NOT NULL,
    lesson_id integer NOT NULL
);


ALTER TABLE public."lessonPostRel" OWNER TO postgres;

--
-- Name: post; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.post (
    id integer NOT NULL,
    title text NOT NULL,
    content text NOT NULL,
    git_link text NOT NULL,
    created_user_id integer NOT NULL
);


ALTER TABLE public.post OWNER TO postgres;

--
-- Name: project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project (
    id integer NOT NULL,
    title text NOT NULL,
    content text NOT NULL,
    git_link text NOT NULL,
    acces_token text NOT NULL,
    path_folder text NOT NULL,
    user_group_id integer NOT NULL,
    created_user_id integer NOT NULL
);


ALTER TABLE public.project OWNER TO postgres;

--
-- Name: projectPostRel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."projectPostRel" (
    id integer NOT NULL,
    post_id integer NOT NULL,
    project_id integer NOT NULL
);


ALTER TABLE public."projectPostRel" OWNER TO postgres;

--
-- Name: reponse; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reponse (
    id integer NOT NULL,
    post_id integer NOT NULL,
    content integer NOT NULL,
    created_user_id integer NOT NULL
);


ALTER TABLE public.reponse OWNER TO postgres;

--
-- Name: tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tag (
    id integer NOT NULL,
    title text NOT NULL,
    description text NOT NULL
);


ALTER TABLE public.tag OWNER TO postgres;

--
-- Name: tagIdefluxRel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."tagIdefluxRel" (
    id integer NOT NULL,
    ideflux_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public."tagIdefluxRel" OWNER TO postgres;

--
-- Name: tagLessonRel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."tagLessonRel" (
    id integer NOT NULL,
    tag_id integer NOT NULL,
    lesson_id integer NOT NULL
);


ALTER TABLE public."tagLessonRel" OWNER TO postgres;

--
-- Name: tagProjectRel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."tagProjectRel" (
    id integer NOT NULL,
    tag_id integer NOT NULL,
    project_id integer NOT NULL
);


ALTER TABLE public."tagProjectRel" OWNER TO postgres;

--
-- Name: tagUserRel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."tagUserRel" (
    id integer NOT NULL,
    user_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public."tagUserRel" OWNER TO postgres;

--
-- Name: tp; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tp (
    id integer NOT NULL,
    title text NOT NULL,
    content text NOT NULL,
    lesson_id integer NOT NULL,
    created_user integer NOT NULL,
    user_group_id integer NOT NULL
);


ALTER TABLE public.tp OWNER TO postgres;

--
-- Name: tpPostRel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."tpPostRel" (
    id integer NOT NULL,
    post_id integer NOT NULL,
    tp_id integer NOT NULL
);


ALTER TABLE public."tpPostRel" OWNER TO postgres;

--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    pseudo text NOT NULL,
    email text NOT NULL,
    password text NOT NULL,
    score integer DEFAULT 0
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: userGroup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."userGroup" (
    id integer NOT NULL,
    title text NOT NULL
);


ALTER TABLE public."userGroup" OWNER TO postgres;

--
-- Name: userGroupRel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."userGroupRel" (
    id integer NOT NULL,
    user_id integer NOT NULL,
    user_group_id integer NOT NULL
);


ALTER TABLE public."userGroupRel" OWNER TO postgres;

--
-- Name: userGroup_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public."userGroup" ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."userGroup_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public."user" ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: vote; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vote (
    id integer NOT NULL,
    is_up boolean NOT NULL,
    post_id integer NOT NULL,
    reponse_id integer NOT NULL,
    created_user_id integer NOT NULL
);


ALTER TABLE public.vote OWNER TO postgres;

--
-- Data for Name: commentaire; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.commentaire (id, content, created_user_id, reponse_id, post_id) FROM stdin;
\.


--
-- Data for Name: document; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.document (id, title, type, lesson_id, project_id, created_user_id) FROM stdin;
\.


--
-- Data for Name: ideflux; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ideflux (id, post_id) FROM stdin;
\.


--
-- Data for Name: lesson; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lesson (id, title, content, user_group_id, created_user_id) FROM stdin;
\.


--
-- Data for Name: lessonPostRel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."lessonPostRel" (id, post_id, lesson_id) FROM stdin;
\.


--
-- Data for Name: post; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.post (id, title, content, git_link, created_user_id) FROM stdin;
\.


--
-- Data for Name: project; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.project (id, title, content, git_link, acces_token, path_folder, user_group_id, created_user_id) FROM stdin;
\.


--
-- Data for Name: projectPostRel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."projectPostRel" (id, post_id, project_id) FROM stdin;
\.


--
-- Data for Name: reponse; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reponse (id, post_id, content, created_user_id) FROM stdin;
\.


--
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tag (id, title, description) FROM stdin;
\.


--
-- Data for Name: tagIdefluxRel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."tagIdefluxRel" (id, ideflux_id, tag_id) FROM stdin;
\.


--
-- Data for Name: tagLessonRel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."tagLessonRel" (id, tag_id, lesson_id) FROM stdin;
\.


--
-- Data for Name: tagProjectRel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."tagProjectRel" (id, tag_id, project_id) FROM stdin;
\.


--
-- Data for Name: tagUserRel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."tagUserRel" (id, user_id, tag_id) FROM stdin;
\.


--
-- Data for Name: tp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tp (id, title, content, lesson_id, created_user, user_group_id) FROM stdin;
\.


--
-- Data for Name: tpPostRel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."tpPostRel" (id, post_id, tp_id) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, pseudo, email, password, score) FROM stdin;
\.


--
-- Data for Name: userGroup; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."userGroup" (id, title) FROM stdin;
\.


--
-- Data for Name: userGroupRel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."userGroupRel" (id, user_id, user_group_id) FROM stdin;
\.


--
-- Data for Name: vote; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vote (id, is_up, post_id, reponse_id, created_user_id) FROM stdin;
\.


--
-- Name: userGroup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."userGroup_id_seq"', 1, false);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 1, false);


--
-- Name: commentaire commentaire_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commentaire
    ADD CONSTRAINT commentaire_pkey PRIMARY KEY (id);


--
-- Name: document document_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT document_pkey PRIMARY KEY (id);


--
-- Name: ideflux ideflux_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ideflux
    ADD CONSTRAINT ideflux_pkey PRIMARY KEY (id);


--
-- Name: lessonPostRel lessonPostRel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."lessonPostRel"
    ADD CONSTRAINT "lessonPostRel_pkey" PRIMARY KEY (id);


--
-- Name: lesson lesson_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lesson
    ADD CONSTRAINT lesson_pkey PRIMARY KEY (id);


--
-- Name: post post_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_pkey PRIMARY KEY (id);


--
-- Name: projectPostRel projectPostRel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."projectPostRel"
    ADD CONSTRAINT "projectPostRel_pkey" PRIMARY KEY (id);


--
-- Name: project project_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_pkey PRIMARY KEY (id);


--
-- Name: reponse reponse_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reponse
    ADD CONSTRAINT reponse_pkey PRIMARY KEY (id);


--
-- Name: tagIdefluxRel tagIdefluxRel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tagIdefluxRel"
    ADD CONSTRAINT "tagIdefluxRel_pkey" PRIMARY KEY (id);


--
-- Name: tagProjectRel tagProjectRel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tagProjectRel"
    ADD CONSTRAINT "tagProjectRel_pkey" PRIMARY KEY (id);


--
-- Name: tagUserRel tagUserRel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tagUserRel"
    ADD CONSTRAINT "tagUserRel_pkey" PRIMARY KEY (id);


--
-- Name: tag tag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (id);


--
-- Name: tpPostRel tpPostRel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tpPostRel"
    ADD CONSTRAINT "tpPostRel_pkey" PRIMARY KEY (id);


--
-- Name: tp tp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tp
    ADD CONSTRAINT tp_pkey PRIMARY KEY (id);


--
-- Name: userGroupRel userGroupRel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."userGroupRel"
    ADD CONSTRAINT "userGroupRel_pkey" PRIMARY KEY (id);


--
-- Name: userGroup userGroup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."userGroup"
    ADD CONSTRAINT "userGroup_pkey" PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: vote vote_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vote
    ADD CONSTRAINT vote_pkey PRIMARY KEY (id);


--
-- Name: commentaire commentaire_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commentaire
    ADD CONSTRAINT commentaire_created_user_id_fkey FOREIGN KEY (created_user_id) REFERENCES public."user"(id);


--
-- Name: commentaire commentaire_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commentaire
    ADD CONSTRAINT commentaire_post_id_fkey FOREIGN KEY (post_id) REFERENCES public.post(id);


--
-- Name: commentaire commentaire_reponse_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.commentaire
    ADD CONSTRAINT commentaire_reponse_id_fkey FOREIGN KEY (reponse_id) REFERENCES public.reponse(id);


--
-- Name: document document_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT document_created_user_id_fkey FOREIGN KEY (created_user_id) REFERENCES public."user"(id);


--
-- Name: document document_lesson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT document_lesson_id_fkey FOREIGN KEY (lesson_id) REFERENCES public.lesson(id);


--
-- Name: document document_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT document_project_id_fkey FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- Name: ideflux ideflux_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ideflux
    ADD CONSTRAINT ideflux_post_id_fkey FOREIGN KEY (post_id) REFERENCES public.post(id);


--
-- Name: lessonPostRel lessonPostRel_lesson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."lessonPostRel"
    ADD CONSTRAINT "lessonPostRel_lesson_id_fkey" FOREIGN KEY (lesson_id) REFERENCES public.lesson(id);


--
-- Name: lessonPostRel lessonPostRel_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."lessonPostRel"
    ADD CONSTRAINT "lessonPostRel_post_id_fkey" FOREIGN KEY (post_id) REFERENCES public.post(id);


--
-- Name: lesson lesson_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lesson
    ADD CONSTRAINT lesson_created_user_id_fkey FOREIGN KEY (created_user_id) REFERENCES public."user"(id);


--
-- Name: lesson lesson_user_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lesson
    ADD CONSTRAINT lesson_user_group_id_fkey FOREIGN KEY (user_group_id) REFERENCES public."userGroup"(id);


--
-- Name: post post_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_created_user_id_fkey FOREIGN KEY (created_user_id) REFERENCES public."user"(id);


--
-- Name: projectPostRel projectPostRel_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."projectPostRel"
    ADD CONSTRAINT "projectPostRel_post_id_fkey" FOREIGN KEY (post_id) REFERENCES public.post(id);


--
-- Name: projectPostRel projectPostRel_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."projectPostRel"
    ADD CONSTRAINT "projectPostRel_project_id_fkey" FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- Name: project project_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_created_user_id_fkey FOREIGN KEY (created_user_id) REFERENCES public."user"(id);


--
-- Name: project project_user_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_user_group_id_fkey FOREIGN KEY (user_group_id) REFERENCES public."userGroup"(id);


--
-- Name: reponse reponse_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reponse
    ADD CONSTRAINT reponse_created_user_id_fkey FOREIGN KEY (created_user_id) REFERENCES public."user"(id);


--
-- Name: reponse reponse_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reponse
    ADD CONSTRAINT reponse_post_id_fkey FOREIGN KEY (post_id) REFERENCES public.post(id);


--
-- Name: tagIdefluxRel tagIdefluxRel_ideflux_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tagIdefluxRel"
    ADD CONSTRAINT "tagIdefluxRel_ideflux_id_fkey" FOREIGN KEY (ideflux_id) REFERENCES public.ideflux(id);


--
-- Name: tagIdefluxRel tagIdefluxRel_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tagIdefluxRel"
    ADD CONSTRAINT "tagIdefluxRel_tag_id_fkey" FOREIGN KEY (tag_id) REFERENCES public.tag(id);


--
-- Name: tagLessonRel tagLessonRel_lesson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tagLessonRel"
    ADD CONSTRAINT "tagLessonRel_lesson_id_fkey" FOREIGN KEY (lesson_id) REFERENCES public.lesson(id);


--
-- Name: tagLessonRel tagLessonRel_lesson_id_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tagLessonRel"
    ADD CONSTRAINT "tagLessonRel_lesson_id_fkey1" FOREIGN KEY (lesson_id) REFERENCES public.lesson(id) NOT VALID;


--
-- Name: tagProjectRel tagProjectRel_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tagProjectRel"
    ADD CONSTRAINT "tagProjectRel_project_id_fkey" FOREIGN KEY (project_id) REFERENCES public.project(id);


--
-- Name: tagProjectRel tagProjectRel_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tagProjectRel"
    ADD CONSTRAINT "tagProjectRel_tag_id_fkey" FOREIGN KEY (tag_id) REFERENCES public.tag(id);


--
-- Name: tagUserRel tagUserRel_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tagUserRel"
    ADD CONSTRAINT "tagUserRel_tag_id_fkey" FOREIGN KEY (tag_id) REFERENCES public.tag(id);


--
-- Name: tagUserRel tagUserRel_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tagUserRel"
    ADD CONSTRAINT "tagUserRel_user_id_fkey" FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: tpPostRel tpPostRel_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tpPostRel"
    ADD CONSTRAINT "tpPostRel_post_id_fkey" FOREIGN KEY (post_id) REFERENCES public.post(id);


--
-- Name: tpPostRel tpPostRel_tp_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."tpPostRel"
    ADD CONSTRAINT "tpPostRel_tp_id_fkey" FOREIGN KEY (tp_id) REFERENCES public.tp(id);


--
-- Name: tp tp_created_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tp
    ADD CONSTRAINT tp_created_user_fkey FOREIGN KEY (created_user) REFERENCES public."user"(id);


--
-- Name: tp tp_lesson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tp
    ADD CONSTRAINT tp_lesson_id_fkey FOREIGN KEY (lesson_id) REFERENCES public.lesson(id);


--
-- Name: tp tp_user_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tp
    ADD CONSTRAINT tp_user_group_id_fkey FOREIGN KEY (user_group_id) REFERENCES public."userGroup"(id);


--
-- Name: userGroupRel userGroupRel_user_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."userGroupRel"
    ADD CONSTRAINT "userGroupRel_user_group_id_fkey" FOREIGN KEY (user_group_id) REFERENCES public."userGroup"(id) NOT VALID;


--
-- Name: userGroupRel userGroupRel_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."userGroupRel"
    ADD CONSTRAINT "userGroupRel_user_id_fkey" FOREIGN KEY (user_id) REFERENCES public."user"(id) NOT VALID;


--
-- Name: vote vote_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vote
    ADD CONSTRAINT vote_created_user_id_fkey FOREIGN KEY (created_user_id) REFERENCES public."user"(id);


--
-- Name: vote vote_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vote
    ADD CONSTRAINT vote_post_id_fkey FOREIGN KEY (post_id) REFERENCES public.post(id);


--
-- Name: vote vote_reponse_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vote
    ADD CONSTRAINT vote_reponse_id_fkey FOREIGN KEY (reponse_id) REFERENCES public.reponse(id);


--
-- PostgreSQL database dump complete
--