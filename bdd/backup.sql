--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Ubuntu 13.2-1.pgdg18.04+1)
-- Dumped by pg_dump version 13.2 (Ubuntu 13.2-1.pgdg18.04+1)

-- Started on 2021-05-06 21:52:45 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 215 (class 1259 OID 37260)
-- Name: Comments; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."Comments" (
    id integer NOT NULL,
    content text,
    post_id integer,
    reponse_id integer,
    created_user_id integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Comments" OWNER TO eisti;

--
-- TOC entry 214 (class 1259 OID 37258)
-- Name: Comments_id_seq; Type: SEQUENCE; Schema: public; Owner: eisti
--

CREATE SEQUENCE public."Comments_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Comments_id_seq" OWNER TO eisti;

--
-- TOC entry 3204 (class 0 OID 0)
-- Dependencies: 214
-- Name: Comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eisti
--

ALTER SEQUENCE public."Comments_id_seq" OWNED BY public."Comments".id;


--
-- TOC entry 227 (class 1259 OID 37418)
-- Name: Documents; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."Documents" (
    id integer NOT NULL,
    title text,
    type integer,
    lesson_id integer,
    project_id integer,
    created_user_id integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Documents" OWNER TO eisti;

--
-- TOC entry 226 (class 1259 OID 37416)
-- Name: Documents_id_seq; Type: SEQUENCE; Schema: public; Owner: eisti
--

CREATE SEQUENCE public."Documents_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Documents_id_seq" OWNER TO eisti;

--
-- TOC entry 3205 (class 0 OID 0)
-- Dependencies: 226
-- Name: Documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eisti
--

ALTER SEQUENCE public."Documents_id_seq" OWNED BY public."Documents".id;


--
-- TOC entry 201 (class 1259 OID 37129)
-- Name: Groups; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."Groups" (
    id integer NOT NULL,
    title text,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Groups" OWNER TO eisti;

--
-- TOC entry 200 (class 1259 OID 37127)
-- Name: Groups_id_seq; Type: SEQUENCE; Schema: public; Owner: eisti
--

CREATE SEQUENCE public."Groups_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Groups_id_seq" OWNER TO eisti;

--
-- TOC entry 3206 (class 0 OID 0)
-- Dependencies: 200
-- Name: Groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eisti
--

ALTER SEQUENCE public."Groups_id_seq" OWNED BY public."Groups".id;


--
-- TOC entry 229 (class 1259 OID 37444)
-- Name: Idefluxes; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."Idefluxes" (
    id integer NOT NULL,
    post_id integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Idefluxes" OWNER TO eisti;

--
-- TOC entry 228 (class 1259 OID 37442)
-- Name: Idefluxes_id_seq; Type: SEQUENCE; Schema: public; Owner: eisti
--

CREATE SEQUENCE public."Idefluxes_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Idefluxes_id_seq" OWNER TO eisti;

--
-- TOC entry 3207 (class 0 OID 0)
-- Dependencies: 228
-- Name: Idefluxes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eisti
--

ALTER SEQUENCE public."Idefluxes_id_seq" OWNED BY public."Idefluxes".id;


--
-- TOC entry 231 (class 1259 OID 37470)
-- Name: LessonPosts; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."LessonPosts" (
    post_id integer NOT NULL,
    lesson_id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."LessonPosts" OWNER TO eisti;

--
-- TOC entry 217 (class 1259 OID 37286)
-- Name: Lessons; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."Lessons" (
    id integer NOT NULL,
    title text,
    content text,
    group_id integer,
    created_user_id integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Lessons" OWNER TO eisti;

--
-- TOC entry 216 (class 1259 OID 37284)
-- Name: Lessons_id_seq; Type: SEQUENCE; Schema: public; Owner: eisti
--

CREATE SEQUENCE public."Lessons_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Lessons_id_seq" OWNER TO eisti;

--
-- TOC entry 3208 (class 0 OID 0)
-- Dependencies: 216
-- Name: Lessons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eisti
--

ALTER SEQUENCE public."Lessons_id_seq" OWNED BY public."Lessons".id;


--
-- TOC entry 209 (class 1259 OID 37200)
-- Name: Posts; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."Posts" (
    id integer NOT NULL,
    title text,
    content text,
    git_link text,
    created_user_id integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Posts" OWNER TO eisti;

--
-- TOC entry 208 (class 1259 OID 37198)
-- Name: Posts_id_seq; Type: SEQUENCE; Schema: public; Owner: eisti
--

CREATE SEQUENCE public."Posts_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Posts_id_seq" OWNER TO eisti;

--
-- TOC entry 3209 (class 0 OID 0)
-- Dependencies: 208
-- Name: Posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eisti
--

ALTER SEQUENCE public."Posts_id_seq" OWNED BY public."Posts".id;


--
-- TOC entry 223 (class 1259 OID 37371)
-- Name: ProjectPosts; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."ProjectPosts" (
    post_id integer NOT NULL,
    project_id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."ProjectPosts" OWNER TO eisti;

--
-- TOC entry 222 (class 1259 OID 37350)
-- Name: Projects; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."Projects" (
    id integer NOT NULL,
    title text,
    content text,
    git_link text,
    access_token text,
    path_folder text,
    group_id integer,
    created_user_id integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Projects" OWNER TO eisti;

--
-- TOC entry 221 (class 1259 OID 37348)
-- Name: Projects_id_seq; Type: SEQUENCE; Schema: public; Owner: eisti
--

CREATE SEQUENCE public."Projects_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Projects_id_seq" OWNER TO eisti;

--
-- TOC entry 3210 (class 0 OID 0)
-- Dependencies: 221
-- Name: Projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eisti
--

ALTER SEQUENCE public."Projects_id_seq" OWNED BY public."Projects".id;


--
-- TOC entry 211 (class 1259 OID 37216)
-- Name: Responses; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."Responses" (
    id integer NOT NULL,
    content text,
    post_id integer,
    created_user_id integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Responses" OWNER TO eisti;

--
-- TOC entry 210 (class 1259 OID 37214)
-- Name: Responses_id_seq; Type: SEQUENCE; Schema: public; Owner: eisti
--

CREATE SEQUENCE public."Responses_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Responses_id_seq" OWNER TO eisti;

--
-- TOC entry 3211 (class 0 OID 0)
-- Dependencies: 210
-- Name: Responses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eisti
--

ALTER SEQUENCE public."Responses_id_seq" OWNED BY public."Responses".id;


--
-- TOC entry 230 (class 1259 OID 37455)
-- Name: TagIdefluxes; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."TagIdefluxes" (
    tag_id integer NOT NULL,
    ideflux_id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."TagIdefluxes" OWNER TO eisti;

--
-- TOC entry 225 (class 1259 OID 37401)
-- Name: TagLessons; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."TagLessons" (
    tag_id integer NOT NULL,
    lesson_id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."TagLessons" OWNER TO eisti;

--
-- TOC entry 224 (class 1259 OID 37386)
-- Name: TagProjects; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."TagProjects" (
    tag_id integer NOT NULL,
    project_id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."TagProjects" OWNER TO eisti;

--
-- TOC entry 207 (class 1259 OID 37183)
-- Name: TagUsers; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."TagUsers" (
    user_id integer NOT NULL,
    tag_id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."TagUsers" OWNER TO eisti;

--
-- TOC entry 206 (class 1259 OID 37172)
-- Name: Tags; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."Tags" (
    id integer NOT NULL,
    title text,
    description text,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Tags" OWNER TO eisti;

--
-- TOC entry 205 (class 1259 OID 37170)
-- Name: Tags_id_seq; Type: SEQUENCE; Schema: public; Owner: eisti
--

CREATE SEQUENCE public."Tags_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Tags_id_seq" OWNER TO eisti;

--
-- TOC entry 3212 (class 0 OID 0)
-- Dependencies: 205
-- Name: Tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eisti
--

ALTER SEQUENCE public."Tags_id_seq" OWNED BY public."Tags".id;


--
-- TOC entry 220 (class 1259 OID 37333)
-- Name: TpPosts; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."TpPosts" (
    post_id integer NOT NULL,
    tp_id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."TpPosts" OWNER TO eisti;

--
-- TOC entry 219 (class 1259 OID 37307)
-- Name: Tps; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."Tps" (
    id integer NOT NULL,
    title text,
    content text,
    lesson_id integer,
    group_id integer,
    created_user_id integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Tps" OWNER TO eisti;

--
-- TOC entry 218 (class 1259 OID 37305)
-- Name: Tps_id_seq; Type: SEQUENCE; Schema: public; Owner: eisti
--

CREATE SEQUENCE public."Tps_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Tps_id_seq" OWNER TO eisti;

--
-- TOC entry 3213 (class 0 OID 0)
-- Dependencies: 218
-- Name: Tps_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eisti
--

ALTER SEQUENCE public."Tps_id_seq" OWNED BY public."Tps".id;


--
-- TOC entry 204 (class 1259 OID 37155)
-- Name: UserGroups; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."UserGroups" (
    user_id integer NOT NULL,
    group_id integer NOT NULL,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."UserGroups" OWNER TO eisti;

--
-- TOC entry 203 (class 1259 OID 37142)
-- Name: Users; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."Users" (
    id integer NOT NULL,
    pseudo text,
    email text,
    password text,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Users" OWNER TO eisti;

--
-- TOC entry 202 (class 1259 OID 37140)
-- Name: Users_id_seq; Type: SEQUENCE; Schema: public; Owner: eisti
--

CREATE SEQUENCE public."Users_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Users_id_seq" OWNER TO eisti;

--
-- TOC entry 3214 (class 0 OID 0)
-- Dependencies: 202
-- Name: Users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eisti
--

ALTER SEQUENCE public."Users_id_seq" OWNED BY public."Users".id;


--
-- TOC entry 213 (class 1259 OID 37237)
-- Name: Votes; Type: TABLE; Schema: public; Owner: eisti
--

CREATE TABLE public."Votes" (
    id integer NOT NULL,
    is_up boolean,
    post_id integer,
    response_id integer,
    created_user_id integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Votes" OWNER TO eisti;

--
-- TOC entry 212 (class 1259 OID 37235)
-- Name: Votes_id_seq; Type: SEQUENCE; Schema: public; Owner: eisti
--

CREATE SEQUENCE public."Votes_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Votes_id_seq" OWNER TO eisti;

--
-- TOC entry 3215 (class 0 OID 0)
-- Dependencies: 212
-- Name: Votes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: eisti
--

ALTER SEQUENCE public."Votes_id_seq" OWNED BY public."Votes".id;


--
-- TOC entry 2943 (class 2604 OID 37263)
-- Name: Comments id; Type: DEFAULT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Comments" ALTER COLUMN id SET DEFAULT nextval('public."Comments_id_seq"'::regclass);


--
-- TOC entry 2947 (class 2604 OID 37421)
-- Name: Documents id; Type: DEFAULT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Documents" ALTER COLUMN id SET DEFAULT nextval('public."Documents_id_seq"'::regclass);


--
-- TOC entry 2937 (class 2604 OID 37132)
-- Name: Groups id; Type: DEFAULT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Groups" ALTER COLUMN id SET DEFAULT nextval('public."Groups_id_seq"'::regclass);


--
-- TOC entry 2948 (class 2604 OID 37447)
-- Name: Idefluxes id; Type: DEFAULT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Idefluxes" ALTER COLUMN id SET DEFAULT nextval('public."Idefluxes_id_seq"'::regclass);


--
-- TOC entry 2944 (class 2604 OID 37289)
-- Name: Lessons id; Type: DEFAULT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Lessons" ALTER COLUMN id SET DEFAULT nextval('public."Lessons_id_seq"'::regclass);


--
-- TOC entry 2940 (class 2604 OID 37203)
-- Name: Posts id; Type: DEFAULT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Posts" ALTER COLUMN id SET DEFAULT nextval('public."Posts_id_seq"'::regclass);


--
-- TOC entry 2946 (class 2604 OID 37353)
-- Name: Projects id; Type: DEFAULT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Projects" ALTER COLUMN id SET DEFAULT nextval('public."Projects_id_seq"'::regclass);


--
-- TOC entry 2941 (class 2604 OID 37219)
-- Name: Responses id; Type: DEFAULT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Responses" ALTER COLUMN id SET DEFAULT nextval('public."Responses_id_seq"'::regclass);


--
-- TOC entry 2939 (class 2604 OID 37175)
-- Name: Tags id; Type: DEFAULT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Tags" ALTER COLUMN id SET DEFAULT nextval('public."Tags_id_seq"'::regclass);


--
-- TOC entry 2945 (class 2604 OID 37310)
-- Name: Tps id; Type: DEFAULT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Tps" ALTER COLUMN id SET DEFAULT nextval('public."Tps_id_seq"'::regclass);


--
-- TOC entry 2938 (class 2604 OID 37145)
-- Name: Users id; Type: DEFAULT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Users" ALTER COLUMN id SET DEFAULT nextval('public."Users_id_seq"'::regclass);


--
-- TOC entry 2942 (class 2604 OID 37240)
-- Name: Votes id; Type: DEFAULT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Votes" ALTER COLUMN id SET DEFAULT nextval('public."Votes_id_seq"'::regclass);


--
-- TOC entry 3182 (class 0 OID 37260)
-- Dependencies: 215
-- Data for Name: Comments; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."Comments" (id, content, post_id, reponse_id, created_user_id, "createdAt", "updatedAt") FROM stdin;
\.


--
-- TOC entry 3194 (class 0 OID 37418)
-- Dependencies: 227
-- Data for Name: Documents; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."Documents" (id, title, type, lesson_id, project_id, created_user_id, "createdAt", "updatedAt") FROM stdin;
\.


--
-- TOC entry 3168 (class 0 OID 37129)
-- Dependencies: 201
-- Data for Name: Groups; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."Groups" (id, title, "createdAt", "updatedAt") FROM stdin;
\.


--
-- TOC entry 3196 (class 0 OID 37444)
-- Dependencies: 229
-- Data for Name: Idefluxes; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."Idefluxes" (id, post_id, "createdAt", "updatedAt") FROM stdin;
5	10	2021-05-06 18:32:02.351+02	2021-05-06 18:32:02.351+02
6	11	2021-05-06 18:46:42.066+02	2021-05-06 18:46:42.066+02
\.


--
-- TOC entry 3198 (class 0 OID 37470)
-- Dependencies: 231
-- Data for Name: LessonPosts; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."LessonPosts" (post_id, lesson_id, "createdAt", "updatedAt") FROM stdin;
\.


--
-- TOC entry 3184 (class 0 OID 37286)
-- Dependencies: 217
-- Data for Name: Lessons; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."Lessons" (id, title, content, group_id, created_user_id, "createdAt", "updatedAt") FROM stdin;
\.


--
-- TOC entry 3176 (class 0 OID 37200)
-- Dependencies: 209
-- Data for Name: Posts; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."Posts" (id, title, content, git_link, created_user_id, "createdAt", "updatedAt") FROM stdin;
11	node.js server ne démarre pas	<p>Bonjour,</p>\n<p>J'ai essay&eacute; ce code tr&egrave;s simple en &eacute;xecutant la commande 'node server.js'. Le message dans le console.log s'affiche mais pourtant quand je vais sur 127.0.0.1:3000 rien ne s'affiche.</p>\n<p>Code :</p>\n<p>'''</p>\n<p>var http = require('http');</p>\n<p>var server = http.createServer();</p>\n<p>server.on('request', function (req,res) {</p>\n<p>&nbsp;&nbsp;&nbsp; res.writeHeader(200, {'Content-Type' : 'text/plain'});</p>\n<p>&nbsp;&nbsp;&nbsp; res.write('Hello World');</p>\n<p>})</p>\n<p>server.listen(3000);</p>\n<p>console.log("Server running on localhost 3000");</p>\n<p>'''</p>\n<p>&nbsp;</p>\n<p><strong>[EDIT]</strong> Merci baptiste pour la r&eacute;ponse ! Il fallait rajouter res.end() ! :D</p>\n<p>&nbsp;</p>	\N	3	2021-05-06 18:46:41.83+02	2021-05-06 19:21:23.173+02
12	Comment allez-vous héberger votre application ? 	<p>Bonjour, j'aimerais savoir si vous pr&eacute;voyez une solution d'h&eacute;bergement sur votre application ?</p>\n<p>Merci !</p>	\N	3	2021-05-06 21:28:44.775+02	2021-05-06 21:28:44.775+02
14	Attention les console.log	<p>Bonjour,</p>\n<p>Petit conseil, faite attention de ne pas laisser des console.log lors de vos commit sur git !</p>\n<p>Voici un exemple dans data/routes.js</p>\n<p>'''</p>\n<p>app.get('/login', jsonParser, function (req, res) {<br />&nbsp; &nbsp; // Connecting to the database.<br />&nbsp; &nbsp; console.log(req.body.username);<br />&nbsp; &nbsp; console.log(req.body.password);<br />&nbsp; &nbsp; connection.getConnection(function (err, connection) {</p>\n<p>&nbsp; &nbsp; // Executing the MySQL query (select all data from the 'users' table).<br />&nbsp; &nbsp; connection.query('SELECT * FROM Profil', function (error, results, fields) {<br />&nbsp; &nbsp; &nbsp; // If some error occurs, we throw an error.<br />&nbsp; &nbsp; &nbsp; if (error) throw error;<br />&nbsp; &nbsp; &nbsp; &nbsp; console.log(results);<br />&nbsp; &nbsp; &nbsp; &nbsp; for (var i = 0; i &lt; results.length; i++) {<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; console.log(results[i].longitude);<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; results[i].latlng = {latitude:results[i].latitude,longitude:results[i].longitude};<br />&nbsp; &nbsp; &nbsp; &nbsp; }<br />&nbsp; &nbsp; &nbsp; &nbsp; console.log(results);<br />&nbsp; &nbsp; &nbsp; // Getting the 'response' from the database and sending it to our route. This is were the data is.<br />&nbsp; &nbsp; &nbsp; res.send(results)<br />&nbsp; &nbsp; });<br />&nbsp; });<br />});</p>\n<p>'''</p>	\N	4	2021-05-06 21:46:38.757+02	2021-05-06 21:46:38.757+02
10	Problème wifi	<p>Bonjour,</p>\n<p>&nbsp;</p>\n<p>Depuis ce matin je n'ai plus acc&egrave;s &agrave; la wifi de l'&eacute;cole, quelqu'un peut m'aider ?</p>\n<p>&nbsp;</p>\n<p>Merci par avance !</p>	\N	3	2021-05-06 18:32:02.11+02	2021-05-06 18:32:02.11+02
\.


--
-- TOC entry 3190 (class 0 OID 37371)
-- Dependencies: 223
-- Data for Name: ProjectPosts; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."ProjectPosts" (post_id, project_id, "createdAt", "updatedAt") FROM stdin;
12	2	2021-05-06 21:28:44.947+02	2021-05-06 21:28:44.947+02
14	2	2021-05-06 21:46:38.843+02	2021-05-06 21:46:38.843+02
\.


--
-- TOC entry 3189 (class 0 OID 37350)
-- Dependencies: 222
-- Data for Name: Projects; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."Projects" (id, title, content, git_link, access_token, path_folder, group_id, created_user_id, "createdAt", "updatedAt") FROM stdin;
2	Projet ING2 : application mobile	<p>Bonjour,</p>\n<p>Ce projet a pour but de mettre en relation les m&eacute;decins et les rempla&ccedil;ants via une application mobile que nous avons choisi de d&eacute;velopper en react native.</p>\n<p>Vous trouverez toutes les questions relative &agrave; ce projet ici.</p>\n<p>N'h&eacute;sitez pas &agrave; dire tout ce que vous pensez sur le code.</p>	https://gitlab.etude.eisti.fr/legallbapt/projeting2	\N	\N	\N	4	2021-05-06 19:50:11.315+02	2021-05-06 19:50:11.315+02
\.


--
-- TOC entry 3178 (class 0 OID 37216)
-- Dependencies: 211
-- Data for Name: Responses; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."Responses" (id, content, post_id, created_user_id, "createdAt", "updatedAt") FROM stdin;
7	<p>Salut y&eacute;y&eacute;,</p>\n<p>&nbsp;</p>\n<p>Il faut rajouter <em>res.end()</em> &agrave; la fin de <em>res.write()</em>.</p>\n<p>&nbsp;</p>\n<p>Bon coding !</p>	11	4	2021-05-06 19:12:46.049+02	2021-05-06 19:12:46.049+02
8	<p>Bonjour,</p>\n<p>Question tr&egrave;s int&eacute;ressante, nous avions pr&eacute;vu d'utiliser des VPS chez OVH, or pour l'instant, l'incident &agrave; Strasbourg nous bloque.</p>\n<p>Cependant, on utilisera 2 VPS, un pour le front et un pour le back.</p>\n<p>Ciao !</p>	12	4	2021-05-06 21:31:39.688+02	2021-05-06 21:31:39.688+02
\.


--
-- TOC entry 3197 (class 0 OID 37455)
-- Dependencies: 230
-- Data for Name: TagIdefluxes; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."TagIdefluxes" (tag_id, ideflux_id, "createdAt", "updatedAt") FROM stdin;
9	5	2021-05-06 18:32:02.415+02	2021-05-06 18:32:02.415+02
10	5	2021-05-06 18:32:02.433+02	2021-05-06 18:32:02.433+02
11	6	2021-05-06 19:21:23.228+02	2021-05-06 19:21:23.228+02
12	6	2021-05-06 19:21:23.228+02	2021-05-06 19:21:23.228+02
\.


--
-- TOC entry 3192 (class 0 OID 37401)
-- Dependencies: 225
-- Data for Name: TagLessons; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."TagLessons" (tag_id, lesson_id, "createdAt", "updatedAt") FROM stdin;
\.


--
-- TOC entry 3191 (class 0 OID 37386)
-- Dependencies: 224
-- Data for Name: TagProjects; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."TagProjects" (tag_id, project_id, "createdAt", "updatedAt") FROM stdin;
13	2	2021-05-06 19:50:11.672+02	2021-05-06 19:50:11.672+02
\.


--
-- TOC entry 3174 (class 0 OID 37183)
-- Dependencies: 207
-- Data for Name: TagUsers; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."TagUsers" (user_id, tag_id, "createdAt", "updatedAt") FROM stdin;
4	13	2021-05-06 21:33:55.085+02	2021-05-06 21:33:55.085+02
\.


--
-- TOC entry 3173 (class 0 OID 37172)
-- Dependencies: 206
-- Data for Name: Tags; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."Tags" (id, title, description, "createdAt", "updatedAt") FROM stdin;
9	Wifi	Wifi école	2021-05-06 18:31:36.255+02	2021-05-06 18:31:36.255+02
10	Pau	Campus de Pau	2021-05-06 18:31:57.683+02	2021-05-06 18:31:57.683+02
11	NodeJs	Node.js est une plateforme logicielle libre en JavaScript, orientée vers les applications réseau événementielles hautement concurrentes qui doivent pouvoir monter en charge. Elle utilise la machine virtuelle V8.	2021-05-06 18:46:07.315+02	2021-05-06 18:46:07.315+02
12	Server	back-end	2021-05-06 18:46:35.771+02	2021-05-06 18:46:35.771+02
13	React Native	Framework react pour application mobile	2021-05-06 19:46:55.153+02	2021-05-06 19:46:55.153+02
\.


--
-- TOC entry 3187 (class 0 OID 37333)
-- Dependencies: 220
-- Data for Name: TpPosts; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."TpPosts" (post_id, tp_id, "createdAt", "updatedAt") FROM stdin;
\.


--
-- TOC entry 3186 (class 0 OID 37307)
-- Dependencies: 219
-- Data for Name: Tps; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."Tps" (id, title, content, lesson_id, group_id, created_user_id, "createdAt", "updatedAt") FROM stdin;
\.


--
-- TOC entry 3171 (class 0 OID 37155)
-- Dependencies: 204
-- Data for Name: UserGroups; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."UserGroups" (user_id, group_id, "createdAt", "updatedAt") FROM stdin;
\.


--
-- TOC entry 3170 (class 0 OID 37142)
-- Dependencies: 203
-- Data for Name: Users; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."Users" (id, pseudo, email, password, "createdAt", "updatedAt") FROM stdin;
3	Yéyé	alexandre.disdier@eisti.eu	$2b$10$OsLz1WV7IP.hwo/dYiSmkOv2FEBQMNH9LWgO7gML1HOjZuM6b0xx.	2021-05-06 18:28:48.514+02	2021-05-06 18:28:48.514+02
4	Baptiste LE GALL	baptiste.legall@eisti.eu	$2b$10$MpWVc3Zf65lqCnist4IfR.L6bbSoQrgfiUS.E3pjTGFpkK2f8BD5G	2021-05-06 18:54:29.569+02	2021-05-06 18:54:29.569+02
\.


--
-- TOC entry 3180 (class 0 OID 37237)
-- Dependencies: 213
-- Data for Name: Votes; Type: TABLE DATA; Schema: public; Owner: eisti
--

COPY public."Votes" (id, is_up, post_id, response_id, created_user_id, "createdAt", "updatedAt") FROM stdin;
86	t	\N	7	3	2021-05-06 19:13:17.19+02	2021-05-06 19:13:17.19+02
88	t	11	\N	3	2021-05-06 19:31:58.673+02	2021-05-06 19:31:58.673+02
91	t	11	\N	4	2021-05-06 19:32:54.968+02	2021-05-06 19:37:21.831+02
92	t	12	\N	4	2021-05-06 21:31:49.89+02	2021-05-06 21:31:49.89+02
\.


--
-- TOC entry 3216 (class 0 OID 0)
-- Dependencies: 214
-- Name: Comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eisti
--

SELECT pg_catalog.setval('public."Comments_id_seq"', 1, false);


--
-- TOC entry 3217 (class 0 OID 0)
-- Dependencies: 226
-- Name: Documents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eisti
--

SELECT pg_catalog.setval('public."Documents_id_seq"', 1, false);


--
-- TOC entry 3218 (class 0 OID 0)
-- Dependencies: 200
-- Name: Groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eisti
--

SELECT pg_catalog.setval('public."Groups_id_seq"', 1, false);


--
-- TOC entry 3219 (class 0 OID 0)
-- Dependencies: 228
-- Name: Idefluxes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eisti
--

SELECT pg_catalog.setval('public."Idefluxes_id_seq"', 6, true);


--
-- TOC entry 3220 (class 0 OID 0)
-- Dependencies: 216
-- Name: Lessons_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eisti
--

SELECT pg_catalog.setval('public."Lessons_id_seq"', 1, false);


--
-- TOC entry 3221 (class 0 OID 0)
-- Dependencies: 208
-- Name: Posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eisti
--

SELECT pg_catalog.setval('public."Posts_id_seq"', 14, true);


--
-- TOC entry 3222 (class 0 OID 0)
-- Dependencies: 221
-- Name: Projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eisti
--

SELECT pg_catalog.setval('public."Projects_id_seq"', 2, true);


--
-- TOC entry 3223 (class 0 OID 0)
-- Dependencies: 210
-- Name: Responses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eisti
--

SELECT pg_catalog.setval('public."Responses_id_seq"', 8, true);


--
-- TOC entry 3224 (class 0 OID 0)
-- Dependencies: 205
-- Name: Tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eisti
--

SELECT pg_catalog.setval('public."Tags_id_seq"', 13, true);


--
-- TOC entry 3225 (class 0 OID 0)
-- Dependencies: 218
-- Name: Tps_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eisti
--

SELECT pg_catalog.setval('public."Tps_id_seq"', 1, false);


--
-- TOC entry 3226 (class 0 OID 0)
-- Dependencies: 202
-- Name: Users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eisti
--

SELECT pg_catalog.setval('public."Users_id_seq"', 4, true);


--
-- TOC entry 3227 (class 0 OID 0)
-- Dependencies: 212
-- Name: Votes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: eisti
--

SELECT pg_catalog.setval('public."Votes_id_seq"', 92, true);


--
-- TOC entry 2974 (class 2606 OID 37268)
-- Name: Comments Comments_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Comments"
    ADD CONSTRAINT "Comments_pkey" PRIMARY KEY (id);


--
-- TOC entry 2994 (class 2606 OID 37426)
-- Name: Documents Documents_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Documents"
    ADD CONSTRAINT "Documents_pkey" PRIMARY KEY (id);


--
-- TOC entry 2950 (class 2606 OID 37137)
-- Name: Groups Groups_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Groups"
    ADD CONSTRAINT "Groups_pkey" PRIMARY KEY (id);


--
-- TOC entry 2952 (class 2606 OID 37139)
-- Name: Groups Groups_title_key; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Groups"
    ADD CONSTRAINT "Groups_title_key" UNIQUE (title);


--
-- TOC entry 2996 (class 2606 OID 37449)
-- Name: Idefluxes Idefluxes_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Idefluxes"
    ADD CONSTRAINT "Idefluxes_pkey" PRIMARY KEY (id);


--
-- TOC entry 3000 (class 2606 OID 37474)
-- Name: LessonPosts LessonPosts_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."LessonPosts"
    ADD CONSTRAINT "LessonPosts_pkey" PRIMARY KEY (post_id, lesson_id);


--
-- TOC entry 2976 (class 2606 OID 37294)
-- Name: Lessons Lessons_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Lessons"
    ADD CONSTRAINT "Lessons_pkey" PRIMARY KEY (id);


--
-- TOC entry 2968 (class 2606 OID 37208)
-- Name: Posts Posts_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Posts"
    ADD CONSTRAINT "Posts_pkey" PRIMARY KEY (id);


--
-- TOC entry 2988 (class 2606 OID 37375)
-- Name: ProjectPosts ProjectPosts_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."ProjectPosts"
    ADD CONSTRAINT "ProjectPosts_pkey" PRIMARY KEY (post_id, project_id);


--
-- TOC entry 2984 (class 2606 OID 37358)
-- Name: Projects Projects_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Projects"
    ADD CONSTRAINT "Projects_pkey" PRIMARY KEY (id);


--
-- TOC entry 2986 (class 2606 OID 37360)
-- Name: Projects Projects_title_key; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Projects"
    ADD CONSTRAINT "Projects_title_key" UNIQUE (title);


--
-- TOC entry 2970 (class 2606 OID 37224)
-- Name: Responses Responses_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Responses"
    ADD CONSTRAINT "Responses_pkey" PRIMARY KEY (id);


--
-- TOC entry 2998 (class 2606 OID 37459)
-- Name: TagIdefluxes TagIdefluxes_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TagIdefluxes"
    ADD CONSTRAINT "TagIdefluxes_pkey" PRIMARY KEY (tag_id, ideflux_id);


--
-- TOC entry 2992 (class 2606 OID 37405)
-- Name: TagLessons TagLessons_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TagLessons"
    ADD CONSTRAINT "TagLessons_pkey" PRIMARY KEY (tag_id, lesson_id);


--
-- TOC entry 2990 (class 2606 OID 37390)
-- Name: TagProjects TagProjects_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TagProjects"
    ADD CONSTRAINT "TagProjects_pkey" PRIMARY KEY (tag_id, project_id);


--
-- TOC entry 2966 (class 2606 OID 37187)
-- Name: TagUsers TagUsers_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TagUsers"
    ADD CONSTRAINT "TagUsers_pkey" PRIMARY KEY (user_id, tag_id);


--
-- TOC entry 2962 (class 2606 OID 37180)
-- Name: Tags Tags_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Tags"
    ADD CONSTRAINT "Tags_pkey" PRIMARY KEY (id);


--
-- TOC entry 2964 (class 2606 OID 37182)
-- Name: Tags Tags_title_key; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Tags"
    ADD CONSTRAINT "Tags_title_key" UNIQUE (title);


--
-- TOC entry 2982 (class 2606 OID 37337)
-- Name: TpPosts TpPosts_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TpPosts"
    ADD CONSTRAINT "TpPosts_pkey" PRIMARY KEY (post_id, tp_id);


--
-- TOC entry 2978 (class 2606 OID 37315)
-- Name: Tps Tps_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Tps"
    ADD CONSTRAINT "Tps_pkey" PRIMARY KEY (id);


--
-- TOC entry 2980 (class 2606 OID 37317)
-- Name: Tps Tps_title_key; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Tps"
    ADD CONSTRAINT "Tps_title_key" UNIQUE (title);


--
-- TOC entry 2960 (class 2606 OID 37159)
-- Name: UserGroups UserGroups_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."UserGroups"
    ADD CONSTRAINT "UserGroups_pkey" PRIMARY KEY (user_id, group_id);


--
-- TOC entry 2954 (class 2606 OID 37154)
-- Name: Users Users_email_key; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_email_key" UNIQUE (email);


--
-- TOC entry 2956 (class 2606 OID 37150)
-- Name: Users Users_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY (id);


--
-- TOC entry 2958 (class 2606 OID 37152)
-- Name: Users Users_pseudo_key; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_pseudo_key" UNIQUE (pseudo);


--
-- TOC entry 2972 (class 2606 OID 37242)
-- Name: Votes Votes_pkey; Type: CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Votes"
    ADD CONSTRAINT "Votes_pkey" PRIMARY KEY (id);


--
-- TOC entry 3013 (class 2606 OID 37279)
-- Name: Comments Comments_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Comments"
    ADD CONSTRAINT "Comments_created_user_id_fkey" FOREIGN KEY (created_user_id) REFERENCES public."Users"(id) ON UPDATE CASCADE;


--
-- TOC entry 3011 (class 2606 OID 37269)
-- Name: Comments Comments_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Comments"
    ADD CONSTRAINT "Comments_post_id_fkey" FOREIGN KEY (post_id) REFERENCES public."Posts"(id) ON UPDATE CASCADE;


--
-- TOC entry 3012 (class 2606 OID 37274)
-- Name: Comments Comments_reponse_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Comments"
    ADD CONSTRAINT "Comments_reponse_id_fkey" FOREIGN KEY (reponse_id) REFERENCES public."Responses"(id) ON UPDATE CASCADE;


--
-- TOC entry 3031 (class 2606 OID 37437)
-- Name: Documents Documents_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Documents"
    ADD CONSTRAINT "Documents_created_user_id_fkey" FOREIGN KEY (created_user_id) REFERENCES public."Users"(id) ON UPDATE CASCADE;


--
-- TOC entry 3029 (class 2606 OID 37427)
-- Name: Documents Documents_lesson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Documents"
    ADD CONSTRAINT "Documents_lesson_id_fkey" FOREIGN KEY (lesson_id) REFERENCES public."Lessons"(id) ON UPDATE CASCADE;


--
-- TOC entry 3030 (class 2606 OID 37432)
-- Name: Documents Documents_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Documents"
    ADD CONSTRAINT "Documents_project_id_fkey" FOREIGN KEY (project_id) REFERENCES public."Projects"(id) ON UPDATE CASCADE;


--
-- TOC entry 3032 (class 2606 OID 37450)
-- Name: Idefluxes Idefluxes_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Idefluxes"
    ADD CONSTRAINT "Idefluxes_post_id_fkey" FOREIGN KEY (post_id) REFERENCES public."Posts"(id) ON UPDATE CASCADE;


--
-- TOC entry 3036 (class 2606 OID 37480)
-- Name: LessonPosts LessonPosts_lesson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."LessonPosts"
    ADD CONSTRAINT "LessonPosts_lesson_id_fkey" FOREIGN KEY (lesson_id) REFERENCES public."Lessons"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3035 (class 2606 OID 37475)
-- Name: LessonPosts LessonPosts_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."LessonPosts"
    ADD CONSTRAINT "LessonPosts_post_id_fkey" FOREIGN KEY (post_id) REFERENCES public."Posts"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3015 (class 2606 OID 37300)
-- Name: Lessons Lessons_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Lessons"
    ADD CONSTRAINT "Lessons_created_user_id_fkey" FOREIGN KEY (created_user_id) REFERENCES public."Users"(id) ON UPDATE CASCADE;


--
-- TOC entry 3014 (class 2606 OID 37295)
-- Name: Lessons Lessons_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Lessons"
    ADD CONSTRAINT "Lessons_group_id_fkey" FOREIGN KEY (group_id) REFERENCES public."Groups"(id) ON UPDATE CASCADE;


--
-- TOC entry 3005 (class 2606 OID 37209)
-- Name: Posts Posts_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Posts"
    ADD CONSTRAINT "Posts_created_user_id_fkey" FOREIGN KEY (created_user_id) REFERENCES public."Users"(id) ON UPDATE CASCADE;


--
-- TOC entry 3023 (class 2606 OID 37376)
-- Name: ProjectPosts ProjectPosts_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."ProjectPosts"
    ADD CONSTRAINT "ProjectPosts_post_id_fkey" FOREIGN KEY (post_id) REFERENCES public."Posts"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3024 (class 2606 OID 37381)
-- Name: ProjectPosts ProjectPosts_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."ProjectPosts"
    ADD CONSTRAINT "ProjectPosts_project_id_fkey" FOREIGN KEY (project_id) REFERENCES public."Projects"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3022 (class 2606 OID 37366)
-- Name: Projects Projects_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Projects"
    ADD CONSTRAINT "Projects_created_user_id_fkey" FOREIGN KEY (created_user_id) REFERENCES public."Users"(id) ON UPDATE CASCADE;


--
-- TOC entry 3021 (class 2606 OID 37361)
-- Name: Projects Projects_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Projects"
    ADD CONSTRAINT "Projects_group_id_fkey" FOREIGN KEY (group_id) REFERENCES public."Groups"(id) ON UPDATE CASCADE;


--
-- TOC entry 3007 (class 2606 OID 37230)
-- Name: Responses Responses_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Responses"
    ADD CONSTRAINT "Responses_created_user_id_fkey" FOREIGN KEY (created_user_id) REFERENCES public."Users"(id) ON UPDATE CASCADE;


--
-- TOC entry 3006 (class 2606 OID 37225)
-- Name: Responses Responses_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Responses"
    ADD CONSTRAINT "Responses_post_id_fkey" FOREIGN KEY (post_id) REFERENCES public."Posts"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3034 (class 2606 OID 37465)
-- Name: TagIdefluxes TagIdefluxes_ideflux_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TagIdefluxes"
    ADD CONSTRAINT "TagIdefluxes_ideflux_id_fkey" FOREIGN KEY (ideflux_id) REFERENCES public."Idefluxes"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3033 (class 2606 OID 37460)
-- Name: TagIdefluxes TagIdefluxes_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TagIdefluxes"
    ADD CONSTRAINT "TagIdefluxes_tag_id_fkey" FOREIGN KEY (tag_id) REFERENCES public."Tags"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3028 (class 2606 OID 37411)
-- Name: TagLessons TagLessons_lesson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TagLessons"
    ADD CONSTRAINT "TagLessons_lesson_id_fkey" FOREIGN KEY (lesson_id) REFERENCES public."Lessons"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3027 (class 2606 OID 37406)
-- Name: TagLessons TagLessons_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TagLessons"
    ADD CONSTRAINT "TagLessons_tag_id_fkey" FOREIGN KEY (tag_id) REFERENCES public."Tags"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3026 (class 2606 OID 37396)
-- Name: TagProjects TagProjects_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TagProjects"
    ADD CONSTRAINT "TagProjects_project_id_fkey" FOREIGN KEY (project_id) REFERENCES public."Projects"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3025 (class 2606 OID 37391)
-- Name: TagProjects TagProjects_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TagProjects"
    ADD CONSTRAINT "TagProjects_tag_id_fkey" FOREIGN KEY (tag_id) REFERENCES public."Tags"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3004 (class 2606 OID 37193)
-- Name: TagUsers TagUsers_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TagUsers"
    ADD CONSTRAINT "TagUsers_tag_id_fkey" FOREIGN KEY (tag_id) REFERENCES public."Tags"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3003 (class 2606 OID 37188)
-- Name: TagUsers TagUsers_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TagUsers"
    ADD CONSTRAINT "TagUsers_user_id_fkey" FOREIGN KEY (user_id) REFERENCES public."Users"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3019 (class 2606 OID 37338)
-- Name: TpPosts TpPosts_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TpPosts"
    ADD CONSTRAINT "TpPosts_post_id_fkey" FOREIGN KEY (post_id) REFERENCES public."Posts"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3020 (class 2606 OID 37343)
-- Name: TpPosts TpPosts_tp_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."TpPosts"
    ADD CONSTRAINT "TpPosts_tp_id_fkey" FOREIGN KEY (tp_id) REFERENCES public."Tps"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3018 (class 2606 OID 37328)
-- Name: Tps Tps_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Tps"
    ADD CONSTRAINT "Tps_created_user_id_fkey" FOREIGN KEY (created_user_id) REFERENCES public."Users"(id) ON UPDATE CASCADE;


--
-- TOC entry 3017 (class 2606 OID 37323)
-- Name: Tps Tps_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Tps"
    ADD CONSTRAINT "Tps_group_id_fkey" FOREIGN KEY (group_id) REFERENCES public."Groups"(id) ON UPDATE CASCADE;


--
-- TOC entry 3016 (class 2606 OID 37318)
-- Name: Tps Tps_lesson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Tps"
    ADD CONSTRAINT "Tps_lesson_id_fkey" FOREIGN KEY (lesson_id) REFERENCES public."Lessons"(id) ON UPDATE CASCADE;


--
-- TOC entry 3002 (class 2606 OID 37165)
-- Name: UserGroups UserGroups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."UserGroups"
    ADD CONSTRAINT "UserGroups_group_id_fkey" FOREIGN KEY (group_id) REFERENCES public."Groups"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3001 (class 2606 OID 37160)
-- Name: UserGroups UserGroups_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."UserGroups"
    ADD CONSTRAINT "UserGroups_user_id_fkey" FOREIGN KEY (user_id) REFERENCES public."Users"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3010 (class 2606 OID 37253)
-- Name: Votes Votes_created_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Votes"
    ADD CONSTRAINT "Votes_created_user_id_fkey" FOREIGN KEY (created_user_id) REFERENCES public."Users"(id) ON UPDATE CASCADE;


--
-- TOC entry 3008 (class 2606 OID 37243)
-- Name: Votes Votes_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Votes"
    ADD CONSTRAINT "Votes_post_id_fkey" FOREIGN KEY (post_id) REFERENCES public."Posts"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3009 (class 2606 OID 37248)
-- Name: Votes Votes_response_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: eisti
--

ALTER TABLE ONLY public."Votes"
    ADD CONSTRAINT "Votes_response_id_fkey" FOREIGN KEY (response_id) REFERENCES public."Responses"(id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2021-05-06 21:52:46 CEST

--
-- PostgreSQL database dump complete
--

