import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IdefluxPostComponent } from './components/ideflux-post/ideflux-post.component';
import { ProjectPostComponent } from './components/project-post/project-post.component';
import { ProjectComponent } from './components/project/project.component';
import { AccountComponent } from './pages/account/account.component';
import { IdefluxComponent } from './pages/ideflux/ideflux.component';
import { LessonComponent } from './pages/lesson/lesson.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { TpComponent } from './pages/tp/tp.component';
import { AuthGuardService } from './services/auth.guard.service';


const routes: Routes = [
  {path:'account', component:AccountComponent, canActivate: [AuthGuardService]},
  {path:'ideflux', component:IdefluxComponent},
  {path:'ideflux/:postId', component:IdefluxPostComponent},
  {path:'projects', component:ProjectsComponent},
  {path:'project/:projectId', component:ProjectComponent},
  {path:'project/post/:postId', component:ProjectPostComponent},
  {path:'lesson', component:LessonComponent},
  {path:'tp', component:TpComponent},
  {path:'**', redirectTo:'/ideflux', pathMatch: 'full'}, 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
