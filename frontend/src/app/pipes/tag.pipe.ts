import { Pipe, PipeTransform } from '@angular/core';
import { Tag } from '../models/tag.model';

@Pipe({
  name: 'tagFilter'
})

export class TagPipe implements PipeTransform {

  transform(tags: Tag[], filterTag: string): Tag[] {
    if(filterTag !== ""){
      return tags.filter(x => this.filterTagWithString(x, filterTag))
    } else {
      return tags;
    }
  }

  filterTagWithString(tag: Tag, string: string) : boolean {
    return tag.title.toLowerCase().split(string.toLowerCase()).length > 1;
  }

}
