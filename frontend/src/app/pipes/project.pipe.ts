import { Pipe, PipeTransform } from '@angular/core';
import { Project } from '../models/project.model';
import { Tag } from '../models/tag.model';
import { Maybe } from '../tools/type';

@Pipe({
  name: 'projectFilter'
})
export class ProjectPipe implements PipeTransform {

  transform(projectList: Maybe<Project[]>, filterProject: string): Maybe<Project[]> {
    if(projectList){
      if(filterProject !== ""){
        return projectList.filter(x => this.filterProjectWithString(x, filterProject))
      } else {
        return projectList;
      }
    } else {
      return []
    }
  }

  filterProjectWithString(project: Project, string: string) : boolean {
    return project.title.toLowerCase().split(string).length > 1 || this.filterProjectWithTag(project.tags, string);
  }

  filterProjectWithTag(tag : Tag[], string: string) : boolean {
    return tag.filter(x => x.title.toLowerCase().split(string).length > 1).length >= 1
  }

}
