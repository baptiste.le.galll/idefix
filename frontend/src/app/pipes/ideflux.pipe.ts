import { Pipe, PipeTransform } from '@angular/core';
import { iif } from 'rxjs';
import { Ideflux } from '../models/ideflux.model';
import { Tag } from '../models/tag.model';
import { Maybe } from '../tools/type';

@Pipe({
  name: 'idefluxFilter'
})
export class IdefluxPipe implements PipeTransform {


  transform(idefluxList: Maybe<Ideflux[]>, filterIdeflux: string): Maybe<Ideflux[]> {
    if(idefluxList){
      if(filterIdeflux !== ""){
        return idefluxList.filter(x => this.filterIdefluxWithString(x, filterIdeflux))
      } else {
        return idefluxList;
      }
    } else {
      return []
    }
  }

  filterIdefluxWithString(ideflux: Ideflux, string: string) : boolean {
    return ideflux.post.title.toLowerCase().split(string.toLowerCase()).length > 1 || this.filterIdefluxWithTag(ideflux.tags, string);
  }

  filterIdefluxWithTag(tag : Tag[], string: string) : boolean {
    return tag.filter(x => x.title.toLowerCase().split(string.toLowerCase()).length > 1).length >= 1
  }
}
