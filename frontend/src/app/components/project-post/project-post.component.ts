import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Post } from 'src/app/models/post.model';
import { Project } from 'src/app/models/project.model';
import { Tag } from 'src/app/models/tag.model';
import { Vote } from 'src/app/models/vote.model';
import { Response } from 'src/app/models/response.model';
import { AuthService } from 'src/app/services/auth.service';
import { ProjectService } from 'src/app/services/project.service';
import { VoteService } from 'src/app/services/vote.service';
import { NO_ID } from 'src/app/tools/constants';
import { Maybe } from 'src/app/tools/type';
import { ModalFormProjectComponent } from '../modal-form-project/modal-form-project.component';

@Component({
  selector: 'app-project-post',
  templateUrl: './project-post.component.html',
  styleUrls: ['./project-post.component.scss']
})
export class ProjectPostComponent implements OnInit {

  project    : Maybe<Project>;
  postId     : number;
  post       : Maybe<Post>;
  list       : any[];
  vote       : Maybe<Vote>;
  disableVote: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private projectService: ProjectService,
    private authService   : AuthService,
    private dialog        : MatDialog,
    private voteService   : VoteService
  ) { 
    this.postId = -1
    this.list = []
  }
 
  ngOnInit(): void {
    const postId = this.activatedRoute.snapshot.paramMap.get('postId')
    if (postId){
      this.postId = +postId
      this.getPostFromProject(this.postId);
    }
  }

  onHighlight(e: any) {
    //console.log("onhighlight",e.value)
  }

  getPostFromProject(postId: number) : void {
    this.projectService.getPostFromProject(postId)
    .then((post: Post) => {
      this.post = post
      this.list = this.transformContent()
      this.vote = this.voteService.getVoteByUserId(this.authService.user.id, this.post.votes)
    })
  }

  isLog() : boolean {
    return this.authService.isLogin();
  }

  getProjectTitle() : string {
    if(this.post)
      return this.post.title
    return ''
  }

  getProjectTags() : Tag[] {
    if(this.project)
      return (this.project.tags)
    return []
  }

  getResponseList() : Response[] {
    if(this.post)
      return this.post.responses
    return []
  }

  getProjectContent() : string {
    if(this.post) {
      return this.post.content
    }
    return ""
  }

  getGitLink() : string {
    if (this.post)
      return this.post.gitLink
    return ''  
  }

  transformContentCodeToHtml(content : string) : string {
    if(content.includes("'''")) {
      
    }
    return(content)
  }

  isModify() : boolean {
    if(this.post)
      return (this.post.createdUserId === this.authService.user.id)
    return false
  }

  openDialogProject() {
    const dialogRef = this.dialog.open(ModalFormProjectComponent, {
      height: '90%',
      width: '60%',
      data: {project : this.project, update: true} 
    });

    dialogRef.afterClosed().toPromise()
    .then(
      (projectUpdated : Project) => {
        this.project = projectUpdated
        this.list = this.transformContent()
      }
    )
  }

  isGithubLink() : boolean {
    if(this.post)
      return(this.post.gitLink != "")
    return false
  }

  transformContent() : any[] {
    if (this.post) {
      let l = this.post.content.split("<p>'''</p>")
      let listObject = []
      for (let i = 0; i < l.length; i++) {
        const element = l[i];
        if (i % 2 !== 0) {
          listObject.push({
            isCode : true,
            code : this.transformCodeContent(element)
          })
        } else {
          listObject.push({
            isCode : false,
            text : this.transformTextContent(element)
          })
        }     
      }
      return listObject
    } else {
      return []
    }
  }

  transformCodeContent(code : string) : string {
    let toSplit = code
    if (code[0] === '\n') {
      toSplit = code.slice(1)
    }
    return toSplit
    .split('<br />').join('\n')
    .split('&nbsp;').join('  ')
    .split("</p>").join("")
    .split("<p>").join("")
    .split("</div>").join("")
    .split("<div>").join("")
    .split('&gt;').join(">")
    .split('&lt;').join("<")
  }
  
  transformTextContent(text : string) : string {
    return text.split('<p>&nbsp;</p>\n').join('')
  }

  get isVoteUp() : boolean {
    if(this.vote) {
      return this.vote.isUp;
    }
    return false;
  }

  get isVoteDown() : boolean {
    if(this.vote) {
      return !this.vote.isUp;
    }
    return false;
  }

  getResponseVote() : number {
    let count : number = 0;
    if(this.post) {
      this.post.votes.forEach( x => {
        if(x.isUp) {
          count = count+1;
        } else {
          count = count-1;
        } 
      })
    }
    return count
  }

  
  upVote(isUp : boolean) {
    if (!this.disableVote){
      this.disableVote = true;
      if(this.post) {
        if(this.vote) {
          if(this.vote.isUp !== isUp) {
            this.voteService.updateVote(this.vote, isUp)
            .then((res:Vote) => {
              if(this.post) {
                this.vote = res;
                this.post.votes = this.post.votes.map(x => x.id === res.id ? res : x)
                this.disableVote = false;
              }
            })
          } else {   
            this.voteService.deleteVote(this.vote)
            .then((success:boolean) => {
              if(this.post) {
                if (this.vote)
                  this.post.votes = this.voteService.deleteVoteFromList(this.post.votes, this.vote)
                this.vote = null;
                this.disableVote = false;
              }
            })
          }
        } else {
          this.voteService.addVote(isUp, this.post.id, NO_ID, this.authService.user.id)
          .then((res: Vote) => {
            if(this.post) {
              this.vote = res;
              this.post.votes.push(res)
              this.disableVote = false;
            }
          })
        }
      }
    } else {
      console.log("Tu as cliqué trop vite mec")
    }
  }
}


