import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Tag } from 'src/app/models/tag.model';
import { TagService } from 'src/app/services/tag.service';

@Component({
  selector: 'app-modal-form-tag',
  templateUrl: './modal-form-tag.component.html',
  styleUrls: ['./modal-form-tag.component.scss']
})
export class ModalFormTagComponent implements OnInit {

  creationForm : FormGroup;
  tag: any;
  constructor(
    private dialog : MatDialogRef<ModalFormTagComponent>,
    private tagService : TagService,
    @Inject(MAT_DIALOG_DATA) public data: {title: string}
  ) {
    this.tag = { title : this.data.title}
    this.creationForm = new FormGroup({
      title : new FormControl(this.tag.title, [Validators.required]),
      description : new FormControl(this.tag.description, [Validators.required])
    })
  }

  get disabled() : boolean {
    return !(this.creationForm.valid)
  }

  ngOnInit(): void {
  }

  onCancel() : void {
    this.dialog.close(false)
  }

  onSubmit() : void {
    this.setTagFromForm();
    this.tagService.addTag(this.tag.title, this.tag.description)
    .then(
      (tag : Tag) => {
        this.tag = tag;
        this.dialog.close(tag)
      }
    )
  }

  
  setTagFromForm() : void {
    this.tag.title    = this.creationForm.value.title;
    this.tag.description = this.creationForm.value.description;
  }
}
