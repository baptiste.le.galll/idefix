import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-left-nav-bar',
  templateUrl: './left-nav-bar.component.html',
  styleUrls: ['./left-nav-bar.component.scss']
})
export class LeftNavBarComponent implements OnInit {
  currentUrl: string;

  isIdefluxActive : boolean ;
  isProjectActive: boolean ;
  isTpActive     : boolean ;
  isLessonActive : boolean ;


  constructor(
    private router : Router,
    private authService : AuthService
  ) { 
    this.currentUrl = '';
    this.isIdefluxActive = false ;
    this.isProjectActive = false ;
    this.isTpActive = false ;
    this.isLessonActive = false ;
  }

  ngOnInit(): void {
    this.router.events.forEach(
      (e : any) => {
        if (e instanceof NavigationEnd) {
          this.activeClass(e.url);
        }
      }
    )
  }

  isLog() : boolean {
    return this.authService.isLogin();
  }

  activeClass(route : string) : void {
    switch (route) {
      case "/ideflux":
        this.isIdefluxActive = true;
        this.isProjectActive = false ;
        this.isTpActive = false ;
        this.isLessonActive = false ;
        break;

      case "/projects":
        this.isProjectActive = true;
        this.isIdefluxActive = false ;
        this.isTpActive = false ;
        this.isLessonActive = false ;
        break;

      case "/tp":
        this.isTpActive = true;
        this.isIdefluxActive = false ;
        this.isProjectActive = false ;
        this.isLessonActive = false ;
        break;

      case "/lesson":
        this.isLessonActive = true;
        this.isIdefluxActive = false ;
        this.isProjectActive = false ;
        this.isTpActive = false ;
        break;
      default:
        this.isLessonActive = false;
        this.isIdefluxActive = false ;
        this.isProjectActive = false ;
        this.isTpActive = false ;
        break;
    }
  }

}
