import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Project } from 'src/app/models/project.model';
import { Tag } from 'src/app/models/tag.model';
import { AuthService } from 'src/app/services/auth.service';
import { ProjectService } from 'src/app/services/project.service';
import { TagService } from 'src/app/services/tag.service';
import { map, startWith } from 'rxjs/operators'
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { CREATE_NEW_TAG_STRING, ERROR_TAG_TITLE_TO_TAG_ID } from 'src/app/tools/constants';
import { ModalFormTagComponent } from '../modal-form-tag/modal-form-tag.component';
import { Maybe } from 'src/app/tools/type';

@Component({
  selector: 'app-modal-form-project',
  templateUrl: './modal-form-project.component.html',
  styleUrls: ['./modal-form-project.component.scss']
})
export class ModalFormProjectComponent implements OnInit {

  tags             : Tag[];
  filterTags       : Observable<string[]>;
  currentTagTitles: string[];

  creationForm      : FormGroup;
  project           : any;
  separatorKeysCodes: number[] = [ENTER, COMMA];

  visible    = true;
  selectable = true;
  removable  = true;
  

  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement> | undefined;

  constructor(
    private projectService : ProjectService,
    private authService    : AuthService,
    private router         : Router,
    private dialogRef      : MatDialogRef<ModalFormProjectComponent>,
    private dialog         : MatDialog,
    private tagService     : TagService
    ) {
      this.tags         = [];
      this.project      = {};
      this.creationForm = new FormGroup({
        title   : new FormControl(this.project.title, [Validators.required]),
        content : new FormControl(this.project.content, [Validators.required]),
        git_link: new FormControl(this.project.gitLink),
        access_token: new FormControl(this.project.access_token),
        path_folder : new FormControl(this.project.path_folder),
        tags    : new FormControl(this.project.tags),
      });
      this.filterTags = this.creationForm.controls["tags"].valueChanges.pipe(
          startWith(''),
          map((val) => {
            return this.search(val)
          })
      );
      this.currentTagTitles = [];
  }
  
  ngOnInit(): void {
    this.initTags()
  }
  

  initFilterTags() : void {
    this.filterTags = this.creationForm.controls["tags"].valueChanges.pipe(
      startWith(null),
      map((val) => {
        return this.search(val)
      })
    );
  }

  initTags() : Promise<Tag[]> {
    return this.tagService.getTags()
    .then(
      (tagsRes : Tag[]) => {
        this.tags = tagsRes;
        this.initFilterTags()
        return this.tags;
      }
    )
  }

  onSubmit() : void {
    this.setProjectFromForm();
    this.projectService.addProject(this.project)
    .then(
      (project : Project) => {
        this.project = project;
        this.dialogRef.close(project)
      }
    )
  }

  onCancel() : void {
    this.dialogRef.close(false);
  }

  setProjectFromForm() : void {
    this.project.title          = this.creationForm.value.title;
    this.project.content        = this.creationForm.value.content;
    this.project.createdUserId  = this.authService.user.id;
    this.project.user_group_id  = 1;
    this.project.access_token   = this.creationForm.value.access_token;
    this.project.path_folder    = this.creationForm.value.path_folder;
    this.project.gitLink        = this.creationForm.value.git_link;
    this.project.tags           = this.tagService.getTagsIdFromTagsString(this.currentTagTitles);
  }
    
  search(value: string | null) : string[] { 
    if(value){
      let filter = value.toLowerCase();
      const listOption = this.tags.filter(tag => tag.title.toLowerCase().includes(filter)).map(x => x.title);
      if (listOption.length === 0) {
        return [CREATE_NEW_TAG_STRING]
      } else {
        return listOption
      }
    } else {
      return this.tags.map(x => x.title)
    }
  }

  openDialogNewTag(title : string) {
    const dialogTag = this.dialog.open(ModalFormTagComponent, {
      height: '350px',
      width: '550px',
      data : {
        title : title
      }
    });

    dialogTag.afterClosed().subscribe(
      (tag : Tag) => {
        this.currentTagTitles.push(tag.title)
        this.tags = this.tagService.tags;
      }
    )
  }


  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      const optionValues = this.search(value)
      if (optionValues.length > 0) {
        if (optionValues[0] !== CREATE_NEW_TAG_STRING) {
          this.currentTagTitles.push(optionValues[0]);
        } else {
          this.openDialogNewTag(value)
        }
      }
    }

    if (input) {
      input.value = '';
    }

    this.creationForm.controls['tags'].setValue(null);
  }

  remove(tag: string): void {
    const index = this.currentTagTitles.indexOf(tag);
    if (index >= 0) {
      this.currentTagTitles.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.currentTagTitles.push(event.option.viewValue);
    if(this.tagInput) 
      this.tagInput.nativeElement.value = '';
    this.creationForm.controls['tags'].setValue(null);
  }

}
