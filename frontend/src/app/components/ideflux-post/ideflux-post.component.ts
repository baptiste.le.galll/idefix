import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { HighlightResult } from 'ngx-highlightjs';
import { Ideflux } from 'src/app/models/ideflux.model';
import { Response } from 'src/app/models/response.model';
import { Tag } from 'src/app/models/tag.model';
import { User } from 'src/app/models/user.model';
import { Vote } from 'src/app/models/vote.model';
import { AuthService } from 'src/app/services/auth.service';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { IdefluxService } from 'src/app/services/ideflux.service';
import { VoteService } from 'src/app/services/vote.service';
import { NO_ID } from 'src/app/tools/constants';
import { Maybe } from 'src/app/tools/type';
import { ModalFormIdefluxComponent } from '../modal-form-ideflux/modal-form-ideflux.component';

@Component({
  selector: 'app-ideflux-post',
  templateUrl: './ideflux-post.component.html',
  styleUrls: ['./ideflux-post.component.scss']
})
export class IdefluxPostComponent implements OnInit {

  ideflux    : Maybe<Ideflux>;
  postId     : number;
  list       : any[];
  vote       : Maybe<Vote>;
  disableVote: boolean = false;
  user: Maybe<User>;
  constructor(
    private activatedRoute: ActivatedRoute,
    private idefluxService: IdefluxService,
    private authService   : AuthService,
    private dialog        : MatDialog,
    private voteService   : VoteService,
    private httpRequestService : HttpRequestService
  ) { 
    this.postId = -1
    this.list = []
  }
 
  ngOnInit(): void {
    const postId = this.activatedRoute.snapshot.paramMap.get('postId')
    if (postId){
      this.postId = +postId
      this.getIdefluxByPostId(this.postId);
    }
  }

  onHighlight(e: any) {
    //console.log("onhighlight",e.value)
  }

  getPostId() :number {
    if (this.ideflux)
      return this.ideflux.post.id
    return -1
  }

  initData() {
    if(this.ideflux) {
      const userId = this.ideflux.post.createdUserId;
      this.httpRequestService.get("users/"+userId)
      .then((user: User) => {
        this.user = user
      })
    }
  }

  getCreatedUser() : string {
    if(this.user) {
      return this.user.pseudo
    }
    return "";
  }

  getIdefluxByPostId(postId: number) : void {
    this.idefluxService.getIdefluxByPostId(postId)
    .then((ideflux: Ideflux) => {
      this.ideflux = ideflux
      this.list = this.transformContent()
      this.vote = this.voteService.getVoteByUserId(this.authService.user.id, this.ideflux.post.votes)
      this.initData();
    })
  }

  getIdefluxTitle() : string {
    if(this.ideflux)
      return this.ideflux.post.title
    return ''
  }

  getIdefluxTags() : Tag[] {
    if(this.ideflux)
      return (this.ideflux.tags)
    return []
  }

  getResponseList() : Response[] {
    if(this.ideflux)
      return this.ideflux.post.responses
    return []
  }

  getIdefluxContent() : string {
    if(this.ideflux) {
      return this.ideflux.post.content
    }
    return ""
  }

  transformContentCodeToHtml(content : string) : string {
    if(content.includes("'''")) {
      
    }
    return(content)
  }

  isModify() : boolean {
    if(this.ideflux){
      return (this.ideflux.post.createdUserId === this.authService.user.id)
    }
    return false
  }


  openDialogIdeflux() {
    const dialogRef = this.dialog.open(ModalFormIdefluxComponent, {
      height: '90%',
      width: '60%',
      data: {ideflux : this.ideflux, update: true} 
    });
    
    dialogRef.afterClosed().toPromise()
    .then(
      (idefluxUpdated : Ideflux | boolean) => {
        console.log("idefluxUpdated",idefluxUpdated)
        if(typeof idefluxUpdated !== "boolean") {
          this.ideflux = idefluxUpdated
          this.list = this.transformContent()
        } 
      }
    )
  }

  isLog() : boolean {
    return this.authService.isLogin();
  }

  isGithubLink() : boolean {
    if(this.ideflux)
      return(this.ideflux.post.gitLink != "")
    return false
  }

  transformContent() : any[] {
    if (this.ideflux) {
      let l = this.ideflux.post.content.split("<p>'''</p>")
      let listObject = []
      for (let i = 0; i < l.length; i++) {
        const element = l[i];
        if (i % 2 !== 0) {
          listObject.push({
            isCode : true,
            code : this.transformCodeContent(element)
          })
        } else {
          listObject.push({
            isCode : false,
            text : this.transformTextContent(element)
          })
        }     
      }
      return listObject
    } else {
      return []
    }
  }

  transformCodeContent(code : string) : string {
    let toSplit = code
    if (code[0] === '\n') {
      toSplit = code.slice(1)
    }
    return toSplit
    .split('<br />').join('\n')
    .split('&nbsp;').join('  ')
    .split("</p>").join("")
    .split("<p>").join("")
    .split("</div>").join("")
    .split("<div>").join("")
    .split('&gt;').join(">")
    .split('&lt;').join("<")
  }
  
  transformTextContent(text : string) : string {
    return text.split('<p>&nbsp;</p>\n').join('')
  }

  get isVoteUp() : boolean {
    if(this.vote) {
      return this.vote.isUp;
    }
    return false;
  }

  get isVoteDown() : boolean {
    if(this.vote) {
      return !this.vote.isUp;
    }
    return false;
  }

  getResponseVote() : number {
    let count : number = 0;
    if(this.ideflux) {
      this.ideflux.post.votes.forEach( x => {
        if(x.isUp) {
          count = count+1;
        } else {
          count = count-1;
        } 
      })
    }
    return count
  }

  
  upVote(isUp : boolean) {
    if (!this.disableVote){
      this.disableVote = true;
      if(this.ideflux) {
        if(this.vote) {
          if(this.vote.isUp !== isUp) {
            this.voteService.updateVote(this.vote, isUp)
            .then((res:Vote) => {
              if(this.ideflux) {
                this.vote = res;
                this.ideflux.post.votes = this.ideflux.post.votes.map(x => x.id === res.id ? res : x)
                this.disableVote = false;
              }
            })
          } else {   
            this.voteService.deleteVote(this.vote)
            .then((success:boolean) => {
              if(this.ideflux) {
                if (this.vote)
                  this.ideflux.post.votes = this.voteService.deleteVoteFromList(this.ideflux.post.votes, this.vote)
                this.vote = null;
                this.disableVote = false;
              }
            })
          }
        } else {
          this.voteService.addVote(isUp, this.ideflux.post.id, NO_ID, this.authService.user.id)
          .then((res: Vote) => {
            if(this.ideflux) {
              this.vote = res;
              this.ideflux.post.votes.push(res)
              this.disableVote = false;
            }
          })
        }
      }
    } else {
      console.log("Tu as cliqué trop vite mec")
    }
  }

  getGitLink() : string {
    if (this.ideflux)
      return this.ideflux.post.gitLink

    return ''
  }

}


