import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-modal-form-signin',
  templateUrl: './modal-form-signin.component.html',
  styleUrls: ['./modal-form-signin.component.scss']
})
export class ModalFormSigninComponent implements OnInit {

  creationForm : FormGroup;
  user: any;

  constructor(
    private authService: AuthService,
    private router : Router,
    private dialog : MatDialogRef<ModalFormSigninComponent>
  ) { 
    this.user = {};
    this.creationForm = new FormGroup({
      email    : new FormControl(this.user.email, [Validators.required, Validators.email]),
      password : new FormControl(this.user.password, [Validators.required]),
      passwordConfirm : new FormControl(this.user.passwordConfirm, [Validators.required]),
      pseudo   : new FormControl(this.user.pseudo, [Validators.required])
    }); 
  }

  get disabled() : boolean {
    return !(this.creationForm.valid && this.isSamePassword())
  }

  ngOnInit(): void {
  }
  
  onCancel() : void {
    this.dialog.close(false);
  }

  onSubmit() : void {
    this.setSignUpFromForm();
    this.authService.signIn(this.user.email, this.user.password, this.user.pseudo)
    .then(
      (user : User) => {
        this.user = user;
        this.router.navigate(['/account'])
        this.dialog.close(user)
      }
    )
  }
 

  isSamePassword() : boolean {
    return(this.creationForm.value.password === this.creationForm.value.passwordConfirm);
  }

  setSignUpFromForm() : void {
    this.user.email    = this.creationForm.value.email;
    this.user.password = this.creationForm.value.password;
    this.user.pseudo   = this.creationForm.value.pseudo;
  }

}
