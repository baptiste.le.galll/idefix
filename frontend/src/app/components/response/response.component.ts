import { Component, Input, OnInit } from '@angular/core';
import { Response } from 'src/app/models/response.model';
import { User } from 'src/app/models/user.model';
import { Vote } from 'src/app/models/vote.model';
import { AuthService } from 'src/app/services/auth.service';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { VoteService } from 'src/app/services/vote.service';
import { NO_ID } from 'src/app/tools/constants';
import { Maybe } from 'src/app/tools/type';

@Component({
  selector: 'app-response',
  templateUrl: './response.component.html',
  styleUrls: ['./response.component.scss']
})
export class ResponseComponent implements OnInit {

  @Input()
  response : Maybe<Response>;
  vote: Maybe<Vote>;
  isCheck : boolean;
  disableVote : boolean = false;
  user: Maybe<User>;
  constructor(
    private voteService : VoteService,
    private authService : AuthService,
    private httpRequestService: HttpRequestService
    ) { 
      this.isCheck = false;
  }

  ngOnInit(): void {
    if(this.response) {
      this.vote = this.voteService.getVoteByUserId(this.authService.user.id, this.response.votes)
      this.initData()
    }
  }

  initData() {
    if(this.response) {
      const userId = this.response.createdUserId;
      this.httpRequestService.get("users/"+userId)
      .then((user: User) => {
        this.user = user
      })
    }
  }

  getCreatedUser() : string {
    if(this.user) {
      return this.user.pseudo
    }
    return "";
  }
  getResponseContent() : string {
    if(this.response)
      return this.response.content
    return ""
  }

  get isVoteUp() : boolean {
    if(this.vote) {
      return this.vote.isUp;
    }
    return false;
  }

  get isVoteDown() : boolean {
    if(this.vote) {
      return !this.vote.isUp;
    }
    return false;
  }

  getResponseVote() : number {
    let count : number = 0;
    if(this.response) {
      this.response.votes.forEach( x => {
        if(x.isUp) {
          count = count+1;
        } else {
          count = count-1;
        } 
      })
    }
    return count
  }

  
  upVote(isUp : boolean) {
    if (!this.disableVote){
      this.disableVote = true;
      if(this.response) {
        if(this.vote) {
          if(this.vote.isUp !== isUp) {
            this.voteService.updateVote(this.vote, isUp)
            .then((res:Vote) => {
              if(this.response) {
                this.vote = res;
                this.response.votes = this.response.votes.map(x => x.id === res.id ? res : x)
                this.disableVote = false;
              }
            })
          } else {   
            this.voteService.deleteVote(this.vote)
            .then((success:boolean) => {
              if(this.response) {
                if (this.vote)
                  this.response.votes = this.voteService.deleteVoteFromList(this.response.votes, this.vote)
                this.vote = null;
                this.disableVote = false;
              }
            })
          }
        } else {
          this.voteService.addVote(isUp, NO_ID, this.response.id, this.authService.user.id)
          .then((res: Vote) => {
            if(this.response) {
              this.vote = res;
              this.response.votes.push(res)
              this.disableVote = false;
            }
          })
        }
      }
    } else {
      console.log("Tu as cliqué trop vite mec")
    }
  }


}
