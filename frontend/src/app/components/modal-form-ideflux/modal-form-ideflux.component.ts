import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Ideflux } from 'src/app/models/ideflux.model';
import { Tag } from 'src/app/models/tag.model';
import { AuthService } from 'src/app/services/auth.service';
import { IdefluxService } from 'src/app/services/ideflux.service';
import { TagService } from 'src/app/services/tag.service';
import { map, startWith } from 'rxjs/operators'
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { CREATE_NEW_TAG_STRING, ERROR_TAG_TITLE_TO_TAG_ID } from 'src/app/tools/constants';
import { ModalFormTagComponent } from '../modal-form-tag/modal-form-tag.component';
import { Maybe } from 'src/app/tools/type';

@Component({
  selector: 'app-modal-form-ideflux',
  templateUrl: './modal-form-ideflux.component.html',
  styleUrls: ['./modal-form-ideflux.component.scss']
})
export class ModalFormIdefluxComponent implements OnInit {

  tags             : Tag[];
  filterTags       : Observable<string[]>;
  currentTagTitles: string[];

  creationForm      : FormGroup;
  ideflux           : any;
  separatorKeysCodes: number[] = [ENTER, COMMA];

  visible    = true;
  selectable = true;
  removable  = true;
  

  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement> | undefined;
  
  constructor(
    private idefluxService : IdefluxService,
    private authService    : AuthService,
    private router         : Router,
    private dialogRef      : MatDialogRef<ModalFormIdefluxComponent>,
    private dialog         : MatDialog,
    private tagService     : TagService,
    @Inject(MAT_DIALOG_DATA) public data: {ideflux: Ideflux, update: true}
    ) {
      this.tags         = [];
      this.ideflux      = {post: {}};
      this.creationForm = new FormGroup({
        title   : new FormControl(this.data && this.data.ideflux ? this.data.ideflux.post.title : this.ideflux.post.title , [Validators.required]),
        content : new FormControl(this.data && this.data.ideflux ? this.data.ideflux.post.content : this.ideflux.post.content, [Validators.required]),
        git_link: new FormControl(this.data && this.data.ideflux ? this.data.ideflux.post.gitLink : this.ideflux.post.gitLink),
        tags    : new FormControl(this.data && this.data.ideflux ? this.data.ideflux.tags : this.ideflux.tags),
      });
      this.filterTags = this.creationForm.controls["tags"].valueChanges.pipe(
          startWith(''),
          map((val) => {
            return this.search(val)
          })
      );
      this.currentTagTitles = this.data && this.data.ideflux ? this.data.ideflux.tags.map(x => x.title) : [];
  }
  
  ngOnInit(): void {
    if(this.data) {
      this.ideflux = this.data.ideflux
    }
    this.initTags()
  }
  

  initFilterTags() : void {
    this.filterTags = this.creationForm.controls["tags"].valueChanges.pipe(
      startWith(null),
      map((val) => {
        return this.search(val)
      })
    );
  }

  initTags() : Promise<Tag[]> {
    return this.tagService.getTags()
    .then(
      (tagsRes : Tag[]) => {
        this.tags = tagsRes;
        this.initFilterTags()
        return this.tags;
      }
    )
  }

  onSubmit() : void {
    this.setIdefluxFromForm();
    this.idefluxService.addIdeflux(this.ideflux)
    .then(
      (ideflux : Ideflux) => {
        this.ideflux = ideflux;
        this.router.navigate(['/ideflux'])
        this.dialogRef.close(ideflux)
      }
    )
  }

  onUpdate() : void {
    this.setIdefluxFromForm();
    this.idefluxService.updateIdeflux(this.ideflux)
    .then(
      (ideflux : Ideflux) => {
        this.ideflux = ideflux;
        this.dialogRef.close(ideflux)
      }
    )
  }


  onCancel() : void {
    this.dialogRef.close(false);
  }

  setIdefluxFromForm() : void {
    this.ideflux.post.title          = this.creationForm.value.title;
    this.ideflux.post.content        = this.creationForm.value.content;
    this.ideflux.post.createdUserId  = this.authService.user.id;
    this.ideflux.post.gitLink        = this.creationForm.value.git_link
    this.ideflux.tags                = this.tagService.getTagsIdFromTagsString(this.currentTagTitles);
  }
    
  search(value: string | null) : string[] { 
    if(value){
      let filter = value.toLowerCase();
      const listOption = this.tags.filter(tag => tag.title.toLowerCase().includes(filter)).map(x => x.title);
      if (listOption.length === 0) {
        return [CREATE_NEW_TAG_STRING]
      } else {
        return listOption
      }
    } else {
      return this.tags.map(x => x.title)
    }
  }

  openDialogNewTag(title : string) {
    const dialogTag = this.dialog.open(ModalFormTagComponent, {
      height: '350px',
      width: '550px',
      data : {
        title : title
      }
    });

    dialogTag.afterClosed().subscribe(
      (tag : Tag) => {
        this.currentTagTitles.push(tag.title)
        this.tags = this.tagService.tags;
      }
    )
  }


  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      const optionValues = this.search(value)
      if (optionValues.length > 0) {
        if (optionValues[0] !== CREATE_NEW_TAG_STRING) {
          this.currentTagTitles.push(optionValues[0]);
        } else {
          this.openDialogNewTag(value)
        }
      }
    }

    if (input) {
      input.value = '';
    }

    this.creationForm.controls['tags'].setValue(null);
  }

  remove(tag: string): void {
    const index = this.currentTagTitles.indexOf(tag);
    if (index >= 0) {
      this.currentTagTitles.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.currentTagTitles.push(event.option.viewValue);
    if(this.tagInput) 
      this.tagInput.nativeElement.value = '';
    this.creationForm.controls['tags'].setValue(null);
  }

}
