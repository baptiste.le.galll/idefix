import { Component, Input, OnInit } from '@angular/core';
import { Ideflux } from 'src/app/models/ideflux.model';
import { Tag } from 'src/app/models/tag.model';
import { User } from 'src/app/models/user.model';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { Maybe } from 'src/app/tools/type';

@Component({
  selector: 'app-ideflux-preview',
  templateUrl: './ideflux-preview.component.html',
  styleUrls: ['./ideflux-preview.component.scss']
})
export class IdefluxPreviewComponent implements OnInit {

  @Input() ideflux : Maybe<Ideflux>;
  user: Maybe<User>;
  constructor(
    private httpRequestService : HttpRequestService
  ) { 
    
  }

  ngOnInit(): void {
    this.initData()
  }

  initData() {
    if(this.ideflux) {
      const userId = this.ideflux.post.createdUserId;
      this.httpRequestService.get("users/"+userId)
      .then((user: User) => {
        this.user = user
      })
    }
  }
  getTitle() : string {
    if (this.ideflux)
      return this.ideflux.post.title

    return ''
  }

  getContent() : string {
    if (this.ideflux)
      return this.ideflux.post.content

    return ''
  }

  getCreatedUser() : string {
    if(this.user)
      return this.user.pseudo
    return "";
  }

  getTags() : Tag[] {
    if(this.ideflux) {
      return this.ideflux.tags;
    }
    return [];
  }

  getVotes() : number {
    let count : number = 0;
    if(this.ideflux) {
      this.ideflux.post.votes.forEach( x => {
        if(x.isUp) {
          count = count+1;
        } else {
          count = count-1;
        } 
      })
    }
    return count
  }

  getAnswers() : number {
    if(this.ideflux) {
      return this.ideflux.post.responses.length;
    }
    return 0
  }

}
