import { Component, Input, OnInit } from '@angular/core';
import { Post } from 'src/app/models/post.model';
import { Maybe } from 'src/app/tools/type';

@Component({
  selector: 'app-post-preview',
  templateUrl: './post-preview.component.html',
  styleUrls: ['./post-preview.component.scss']
})
export class PostPreviewComponent implements OnInit {

  @Input() post : Maybe<Post>;

  constructor() { }

  ngOnInit(): void {
  }

  getTitle() : string {
    if (this.post)
      return this.post.title

    return ''
  }

    getContent() : string {
    if (this.post)
      return this.post.content

    return ''
  }

  getVotes() : number {
    let count : number = 0;
    if(this.post) {
      this.post.votes.forEach( x => {
        if(x.isUp) {
          count = count+1;
        } else {
          count = count-1;
        } 
      })
    }
    return count
  }

  getAnswers() : number {
    if(this.post) {
      return this.post.responses.length;
    }
    return 0
  }

}