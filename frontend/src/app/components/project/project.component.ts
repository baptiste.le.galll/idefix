import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Post } from 'src/app/models/post.model';
import { Project } from 'src/app/models/project.model';
import { IdefluxService } from 'src/app/services/ideflux.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Maybe } from 'src/app/tools/type';
import { ActivatedRoute } from '@angular/router';
import { ProjectService } from 'src/app/services/project.service';
import { Tag } from 'src/app/models/tag.model';
import { ModalFormProjectPostComponent } from '../modal-form-project-post/modal-form-project-post.component';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  projectId : number;
  project : Maybe<Project>;

  constructor(
    private router : Router,
    private activatedRoute : ActivatedRoute,
    private projectService : ProjectService,
    private authService : AuthService,
    public dialog: MatDialog
  ) { 
    this.projectId = -1;
  }

  ngOnInit(): void {
    const projectId = this.activatedRoute.snapshot.paramMap.get('projectId')
    if (projectId)
      this.projectId = +projectId
      this.getProject()
  }

  getProjectId() : number {
    return this.projectId
  }

  getProject() : void {
    this.projectService.getProject(this.projectId)
    .then(
      (project : Project) => {
        return this.project = project
      }
    )
  }

  getProjectContent() : string {
    if(this.project)
      return this.project.content
    return ''
    }

  getProjectTitle() : string {
    if(this.project)
      return this.project.title
    return ''  
  }

  getProjectTags() : Tag[] {
    if(this.project)
      return (this.project.tags)
    return []
  }

  getProjectPosts() : Post[] {
    if(this.project)
      return (this.project.posts)
    return []
  }

  isLog() : boolean {
    return this.authService.isLogin();
  }

  openDialogAddPost() {
    const dialogRef = this.dialog.open(ModalFormProjectPostComponent, {
      height: '90%',
      width: '60%',
      data: {project : this.project}
    }).afterClosed().toPromise().then(
      (post :Post | boolean) => {
        if (typeof post !== "boolean")
          this.project?.posts.push(post)
      }
    )
  }

  openPost(post : Post) {
    this.router.navigate(['project/post/', post.id])
  }

  getGitLink() : string {
    if (this.project)
      return this.project.gitLink
    return ''  
  }

}
