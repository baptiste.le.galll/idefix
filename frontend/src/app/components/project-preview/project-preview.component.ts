import { Component, Input, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project.model';
import { Tag } from 'src/app/models/tag.model';
import { Maybe } from 'src/app/tools/type';

@Component({
  selector: 'app-project-preview',
  templateUrl: './project-preview.component.html',
  styleUrls: ['./project-preview.component.scss']
})
export class ProjectPreviewComponent implements OnInit {

  @Input() project : Maybe<Project>;

  constructor() { }

  ngOnInit(): void {
  }

  getTitle() : string {
    if (this.project)
      return this.project.title

    return ''
  }

  getContent() : string {
    if (this.project)
      return this.project.content

    return ''
  }

  getTags() : Tag[] {
    if(this.project) {
      return this.project.tags;
    }
    return [];
  }

  getPost() : number {
    if(this.project) { 
      return this.project.posts.length
    }
    return 0
  }

}
