import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  
  postId : number;

  constructor(
    private activatedRoute : ActivatedRoute
  ) { 
    this.postId = -1;
  }

  ngOnInit(): void {
    const postId = this.activatedRoute.snapshot.paramMap.get('postId')
    if (postId)
      this.postId = +postId
  }

  getPostId() : number {
    return this.postId
  }

}
