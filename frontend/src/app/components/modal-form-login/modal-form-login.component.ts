import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-modal-form-login',
  templateUrl: './modal-form-login.component.html',
  styleUrls: ['./modal-form-login.component.scss']
})
export class ModalFormLoginComponent implements OnInit {

  creationForm : FormGroup;
  user: User;

  errorMessage : string  = "";
  hasError     : boolean = false;

  constructor(
    private authService: AuthService,
    private router : Router,
    private dialog : MatDialogRef<ModalFormLoginComponent>
  ) { 
    this.user = new User();
    this.creationForm = new FormGroup({
      email    : new FormControl(this.user.email, [Validators.required, Validators.email]),
      password : new FormControl(this.user.password, [Validators.required]),
    }); 
  }

  ngOnInit(): void {
  }

  onCancel() : void {
    this.dialog.close(false);
  }

  onSubmit() : void {
    this.setLoginFromForm();
    this.authService.login(this.user.email, this.user.password)
    .then(
      (user : User) => {
        this.user = user;
        this.router.navigate(['/account'])
        this.dialog.close(user)
      }
    ).catch(
      (err : any) => {
        this.hasError = true;
        this.errorMessage =  err.error.error.message;
      }
    )
  }

  setLoginFromForm() : void {
    this.user.email    = this.creationForm.value.email;
    this.user.password = this.creationForm.value.password;
  }

}
