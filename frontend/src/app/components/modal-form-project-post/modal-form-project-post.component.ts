import { Component, ElementRef, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';
import { ProjectService } from 'src/app/services/project.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Post } from 'src/app/models/post.model';
import { Project } from 'src/app/models/project.model';

@Component({
  selector: 'app-modal-form-project-post',
  templateUrl: './modal-form-project-post.component.html',
  styleUrls: ['./modal-form-project-post.component.scss']
})
export class ModalFormProjectPostComponent implements OnInit {
  creationForm      : FormGroup;
  post              : any;
  separatorKeysCodes: number[] = [ENTER, COMMA];

  visible    = true;
  selectable = true;
  removable  = true;

  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement> | undefined;

  constructor(
    private projectService : ProjectService,
    private authService    : AuthService,
    private dialogRef      : MatDialogRef<ModalFormProjectPostComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {project: Project}
    ) {
      this.post         = {};
      this.creationForm = new FormGroup({
        title   : new FormControl(this.post.title, [Validators.required]),
        content : new FormControl(this.post.content, [Validators.required]),
        git_link: new FormControl(this.post.gitLink),
      });
    }
  
  ngOnInit(): void {
  }
  
  onSubmit() : void {
    this.setProjectPostFromForm();
    this.projectService.addPost(this.post,this.data.project)
    .then(
      (post : Post) => {
        this.post = post;
        this.dialogRef.close(post)
      }
    )
  }

  onCancel() : void {
    this.dialogRef.close(false);
  }

  setProjectPostFromForm() : void {
    this.post.title          = this.creationForm.value.title;
    this.post.content        = this.creationForm.value.content;
    this.post.createdUserId  = this.authService.user.id;
    this.post.user_group_id  = 1;
    this.post.gitLink        = this.creationForm.value.git_link;
  }
}
