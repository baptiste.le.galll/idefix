import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { ResponseService } from 'src/app/services/response.service';
import { Maybe } from 'src/app/tools/type';

@Component({
  selector: 'app-form-response',
  templateUrl: './form-response.component.html',
  styleUrls: ['./form-response.component.scss']
})
export class FormResponseComponent implements OnInit {
  creationForm : FormGroup;
  response : any;
  @Input()
  postId: Maybe<number>
  
  constructor(
    private responseService: ResponseService,
    private authService: AuthService
  ) { 
    this.response = {};
    this.creationForm = new FormGroup({
      content    : new FormControl(this.response.content, [Validators.required]),
    }); 
  }

  ngOnInit(): void {
  }

  onSubmit() : void {
    this.setResponseForm();
    if(this.postId)
      this.responseService.addResponse(this.response.content, this.postId, this.authService.user.id)
      .then(
        () => {
          window.location.reload()
        }
      )
  }

  setResponseForm() : void {
    this.response.content = this.creationForm.value.content;
  }

}
