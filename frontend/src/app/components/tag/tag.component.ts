import { Component, Input, OnInit } from '@angular/core';
import { Tag } from 'src/app/models/tag.model';
import { Maybe } from 'src/app/tools/type';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss']
})
export class TagComponent implements OnInit {

  @Input() tag : Maybe<Tag>;

  constructor() { }

  ngOnInit(): void {
  }

  getTitle() : string {
    if (this.tag)
      return this.tag.title

    return ''
  }

  getDescription() : string {
    if (this.tag)
      return this.tag.description

    return ''
  }

}

