import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NavigationEnd, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ModalFormIdefluxComponent } from '../modal-form-ideflux/modal-form-ideflux.component';
import { ModalFormProjectComponent } from '../modal-form-project/modal-form-project.component';
import { ModalFormLoginComponent } from '../modal-form-login/modal-form-login.component';
import { ModalFormSigninComponent } from '../modal-form-signin/modal-form-signin.component';

@Component({
  selector: 'app-top-nav-bar',
  templateUrl: './top-nav-bar.component.html',
  styleUrls: ['./top-nav-bar.component.scss']
})
export class TopNavBarComponent implements OnInit {

  currentUrl : string;

  constructor(private router: Router, public dialog: MatDialog, private authService : AuthService) { 
    this.currentUrl = "";
  }

  get isLog() : boolean {
    return this.authService.isLogin()
  }

  ngOnInit(): void {
    this.router.events.forEach(
      (e: any) => {
        if(e instanceof NavigationEnd) {
          this.currentUrl = e.url.split("/")[1];
        }
      }
    )
  }

  getTranslateUrl() : string {
    switch(this.currentUrl) {
      case "lesson":
        return "cours";

      case "ideflux":
        return "ideflux";
      
      case "account":
        return "profil";

      case "tp":
        return "tp";
      
      case "projects":
        return "projet";

      case "project":
        return "projet";
      
      default:
        return "";

    }
  }

  openDialogIdeflux() {
    switch(this.currentUrl) {
      case "ideflux":
        const dialogRef = this.dialog.open(ModalFormIdefluxComponent, {
          height: '90%',
          width: '60%', 
        });
        break;
      
      case "projects":
        const dialogProject = this.dialog.open(ModalFormProjectComponent, {
          height: '90%',
          width: '60%', 
        });
        break;

      case "project":
        const dialogProject2 = this.dialog.open(ModalFormProjectComponent, {
          height: '90%',
          width: '60%', 
        });
        break;
     
    }
  }

  openDialogSignin() {
    const dialogRef = this.dialog.open(ModalFormSigninComponent, {
      height: '400px',
      width: '600px'
    });
  }

  openDialogLogin() {
    const dialogRef = this.dialog.open(ModalFormLoginComponent, {
      height: '300px',
      width: '600px'
    });
  }

  isRouteAccount() : boolean {
    return this.currentUrl === "account"
  }

  disconnect() : void {
    this.authService.disconnect();
  }


}
