import { Response } from "./response.model";
import { Vote } from "./vote.model";

export class Post {

    id : number;
    title : string;
    content : string;
    votes : Vote[];
    responses : Response[];
    createdUserId : number;
    gitLink : string;
    
    constructor(
        _id? : number,
        _title? : string,
        _content? : string,
        _votes? : Vote[],
        _responses? : Response[],
        _createdUserId? : number,
        _gitLink? : string    
    );

    constructor(
        _id : number,
        _title : string,
        _content : string,
        _votes : Vote[],
        _responses : Response[],
        _createdUserId : number,
        _gitLink? : string
    ) {
        this.id = _id;
        this.title = _title;
        this.content = _content;
        this.votes = _votes;
        this.responses = _responses;
        this.createdUserId = _createdUserId;
        this.gitLink = _gitLink || '';
    }

    asJson() : any {
        return {
            id : this.id,
            title : this.title,
            content : this.content,
            votes : this.votes,
            responses : this.responses,
            created_user_id : this.createdUserId,
            git_link : this.gitLink
        }
    }

    static asPosts(listJson : any[]) : Post[] {
        return listJson.map((json : any) => {
            return Post.asPost(json)
        })
    }

    static asPost(json : any) : Post {
        return new Post(
            json['id'],
            json['title'],
            json['content'],
            json['votes'] ? Vote.asVotes(json['votes']) : [],
            json['responses'] ? Response.asResponses(json['responses']) : [],
            json['created_user_id'],
            json['git_link'] || ''
        )
    }
}
