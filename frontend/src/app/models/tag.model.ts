export class Tag {
    id: number;
    title: string;
    description: string;
    createdAt : Date
    constructor(
        _id?: number,
        _title? : string,
        _description? : string,
        _createdAt? : Date 
    )

    constructor(
        _id: number,
        _title : string,
        _description: string,
        _createdAt : Date 
    ){
        this.id = _id;
        this.title = _title;
        this.description = _description;
        this.createdAt = new Date()
    }

    asJson() : any {
        return {
            id: this.id,
            title: this.title,
            description : this.description
        } 
    }

    static asTag(json : any) : Tag {
        return new Tag(
            json['id'],
            json['title'],
            json['description'],
            json['createdAt']
        )
    }

    static asTags(jsonList : any[]) : Tag[] {
        return jsonList.map(
            (x : any) => Tag.asTag(x)
        )
    }
}
