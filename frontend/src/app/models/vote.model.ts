import { Maybe } from "../tools/type";

export class Vote {
    id : number;
    isUp : boolean;
    postId : Maybe<number>;
    responseId : Maybe<number>;
    createdUserId : number;
    
    constructor(
    _id? : number,
    _isUp? : boolean,
    _postId? : Maybe<number>,
    _responseId? : Maybe<number>,
    _createdUserId? : number,
    );

    constructor(
        _id : number,
        _isUp : boolean,
        _postId : Maybe<number>,
        _responseId : Maybe<number>,
        _createdUserId : number,
    ) {
        this.id = _id; 
        this.isUp = _isUp; 
        this.postId = _postId; 
        this.responseId = _responseId; 
        this.createdUserId = _createdUserId; 
    }

    asJson() : any {
        return {
            id : this.id, 
            is_up : this.isUp, 
            post_id : this.postId, 
            response_id : this.responseId, 
            created_user_id : this.createdUserId, 
        }
    }

    static asVotes(listJson : any[]) : Vote[] {
        return listJson.map((json : any) => {
            return Vote.asVote(json)
        })
    }

    static asVote(json: any) : Vote {
        return new Vote(
            json['id'],
            json['is_up'],
            json['post_id'],
            json['response_id'],
            json['created_user_id'],
        )
    }
    
}
