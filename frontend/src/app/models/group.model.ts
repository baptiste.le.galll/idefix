export class Group {
    id: number;
    title: string;

    constructor(
        _id?: number,
        _title? : string, 
    )

    constructor(
        _id: number,
        _title : string, 
    ){
        this.id = _id;
        this.title = _title;
    }

    asJson() : any {
        return {
            id: this.id,
            title: this.title
        } 
    }

    static asGroup(json : any) : Group {
        return new Group(
            json['id'],
            json['title']
        )
    }

    static asGroups(jsonList : any[]) : Group[] {
        return jsonList.map(
            (x : any) => Group.asGroup(x)
        )
    }
}
