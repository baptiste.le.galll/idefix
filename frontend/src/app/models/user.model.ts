import { Group } from "./group.model";
import { Tag } from "./tag.model";

export class User {
    id: number;
    pseudo: string;
    email: string;
    password: string;
    groups : Group[];
    tags: Tag[];

    constructor(
        _id?: number,
        _pseudo?: string,
        _email?: string,
        _password?: string,
        _groups? : Group[],
        _tags?: Tag[]
    )

    constructor(
        _id: number,
        _pseudo: string,
        _email: string,
        _password: string,
        _groups? : Group[],
        _tags?: Tag[]
    ){
        this.id = _id;
        this.pseudo = _pseudo;
        this.email = _email;
        this.password = _password;
        this.groups = _groups || [];
        this.tags = _tags || [];
    }

    asJson() : any {
       return {
        id : this.id,
        pseudo : this.pseudo,
        email : this.email,
        password : this.password,
        groups : this.groups.map(x => x.asJson()),
        tags : this.tags.map(x => x.asJson()),
       } 
    }

    static asUser(json: any) : User {
        return new User(
            json['id'],
            json['pseudo'],
            json['email'],
            json['password'],
            json['groups'] ? Group.asGroups(json['groups']) : [],
            json['tags'] ? Tag.asTags(json['tags']) : []
        )
    }
}
