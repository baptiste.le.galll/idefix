import { ERROR_DEFAULT_DATE } from "../tools/constants";
import { Post } from "./post.model";
import { Tag } from "./tag.model";

export class Ideflux {

    id: number;
    post: Post;
    tags: Tag[];
    createdAt : Date;

    constructor(
        _id? : number,
        _post? : Post,
        _tags? : Tag[],
        _createdAt? : Date
    )

    constructor(
        _id : number,
        _post : Post,
        _tags : Tag[],
        _createdAt : Date
    ){
        this.id = _id;
        this.post = _post;
        this.tags = _tags;
        this.createdAt = _createdAt
    }

    asJson() : any {
        return {
            id : this.id,
            post : this.post,
            tags : this.tags,
            createdAt : this.createdAt
        }
    }

    static asIdeflux(json : any) : Ideflux {
        return new Ideflux(
            json['id'],
            json['post'] ? Post.asPost(json['post']) : new Post(),
            json['tags'] ? Tag.asTags(json['tags']) : [],
            json['createdAt'] ? new Date(json['createdAt']) : ERROR_DEFAULT_DATE
        )
    }

    static asIdefluxs(jsonList : any[]) : Ideflux[] {
        return jsonList.map(
            (x : any) => Ideflux.asIdeflux(x)
        )
    }
}
