import { Post } from "./post.model";
import { Tag } from "./tag.model";

export class Project {

    id : number;
    title : string;
    content : string;
    gitLink : string;
    access_token : string;
    path_folder : string;
    tags: Tag[];
    posts : Post[];
    group_id : number;
    createdUserId : number;
    
    constructor(
        _id? : number,
        _title? : string,
        _content? : string,
        _access_token? : string,
        _group_id? : number,
        _createdUserId? : number,
        _path_folder? : string,
        _gitLink? : string,
        _tags? : Tag[],
        _posts? : Post[],
    
    );

    constructor(
        _id : number,
        _title : string,
        _content : string,
        _access_token : string,
        _group_id : number,
        _createdUserId : number,
        _path_folder? : string,
        _gitLink? : string,
        _tags? : Tag[],
        _posts? : Post[],
    ) {
        this.id = _id;
        this.title = _title;
        this.content = _content;
        this.access_token = _access_token;
        this.group_id = _group_id;
        this.createdUserId = _createdUserId;
        this.path_folder = _path_folder || '';
        this.gitLink = _gitLink || '';
        this.tags = _tags || [];
        this.posts = _posts || [];
    }

    asJson() : any {
        return {
            id : this.id,
            title : this.title,
            content : this.content,
            access_token : this.access_token,
            group_id : this.group_id,
            created_user_id : this.createdUserId,
            path_folder : this.path_folder, 
            git_link : this.gitLink,
            tags: this.tags,
            posts : this.posts
        }
    }

    static asProject(json : any) : Project {
        return new Project(
            json['id'],
            json['title'],
            json['content'],
            json['access_token'],
            json['group_id'],
            json['created_user_id'],
            json['path_folder'] || '',
            json['git_link'] || '',
            json['tags'] ? Tag.asTags(json['tags']) : [],
            json['posts'] ? Post.asPosts(json['posts']) : []
        )
    }

    static asProjects(jsonList : any[]) : Project[] {
        return jsonList.map(
            (x : any) => Project.asProject(x)
        )
    }

    
}
