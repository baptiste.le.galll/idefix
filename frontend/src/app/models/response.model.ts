import { User } from "./user.model";
import { Vote } from "./vote.model";

export class Response {

    id : number;
    content : string;
    postId : number;
    votes : Vote[];
    user : User;
    createdUserId : number;
    
    constructor(
        _id? : number,
        _content? : string,
        _postId? : number,
        _votes? : Vote[],
        _user? : User,
        _createdUserId? : number, 
    );

    constructor(
        _id : number,
        _content : string,
        _postId : number,
        _votes : Vote[],
        _user : User,
        _createdUserId : number,
    ) {
        this.id = _id;
        this.content = _content;
        this.postId = _postId;
        this.votes = _votes;
        this.user = _user;
        this.createdUserId = _createdUserId;
    }

    asJson() : any {
        return {
            id : this.id,
            content : this.content,
            post_id : this.postId,
            votes : this.votes,
            user : this.user.asJson(),
            createdUserId : this.createdUserId
        }
    }

    static asResponses(listJson : any[]) : Response[] {
        return listJson.map((json : any) => {
            return new Response(
                json['id'],
                json['content'],
                json['post_id'],
                json['votes'] ? Vote.asVotes(json['votes']) : [],
                json['user'] ? User.asUser(json['user']) : new User(),
                json['created_user_id']
            )
        })
    }

    static asResponse(json: any) : Response {
        return new Response(
            json['id'],
            json['content'],
            json['post_id'],
            json['votes'],
            json['user'],
            json['created_user_id']
        )
    }
}