import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LeftNavBarComponent } from './components/left-nav-bar/left-nav-bar.component';
import { TopNavBarComponent } from './components/top-nav-bar/top-nav-bar.component';
import { IdefluxComponent } from './pages/ideflux/ideflux.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { ProjectComponent } from './components/project/project.component';
import { TpComponent } from './pages/tp/tp.component';
import { LessonComponent } from './pages/lesson/lesson.component';
import { AccountComponent } from './pages/account/account.component';
import { IdefluxPreviewComponent } from './components/ideflux-preview/ideflux-preview.component';
import { HttpClientModule } from '@angular/common/http';
import { TagComponent } from './components/tag/tag.component';
import { PostComponent } from './components/post/post.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { ModalFormIdefluxComponent } from './components/modal-form-ideflux/modal-form-ideflux.component';
import { ModalFormProjectComponent } from './components/modal-form-project/modal-form-project.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ModalFormLoginComponent } from './components/modal-form-login/modal-form-login.component';
import { ModalFormSigninComponent } from './components/modal-form-signin/modal-form-signin.component';
import { PostPreviewComponent } from './components/post-preview/post-preview.component';
import { ModalFormTagComponent } from './components/modal-form-tag/modal-form-tag.component';
import { IdefluxPostComponent } from './components/ideflux-post/ideflux-post.component';
import { HighlightPlusModule } from 'ngx-highlightjs/plus';
import { HIGHLIGHT_OPTIONS } from 'ngx-highlightjs';
import { EditorModule } from "@tinymce/tinymce-angular";
import { ProjectPreviewComponent } from './components/project-preview/project-preview.component';
import { ResponseComponent } from './components/response/response.component';
import { FormResponseComponent } from './components/form-response/form-response.component';
import { TagPipe } from './pipes/tag.pipe';
import { ModalFormProjectPostComponent } from './components/modal-form-project-post/modal-form-project-post.component';
import { IdefluxPipe } from './pipes/ideflux.pipe';
import { ProjectPipe } from './pipes/project.pipe';
import { ProjectPostComponent } from './components/project-post/project-post.component';

@NgModule({
  declarations: [
    AppComponent,
    LeftNavBarComponent,
    TopNavBarComponent,
    IdefluxComponent,
    ProjectsComponent,
    ProjectComponent,
    TpComponent,
    LessonComponent,
    AccountComponent,
    PostComponent,
    TagComponent,
    PostPreviewComponent,
    ModalFormIdefluxComponent,
    ModalFormProjectComponent,
    ModalFormProjectPostComponent,
    ModalFormLoginComponent,
    ModalFormSigninComponent,
    IdefluxPreviewComponent,
    ModalFormTagComponent,
    IdefluxPostComponent,
    ProjectPreviewComponent,
    ResponseComponent,
    FormResponseComponent,
    TagPipe,
    IdefluxPipe,
    ProjectPipe,
    ProjectPostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatIconModule,
    HighlightPlusModule,
    EditorModule,
    FormsModule
  ],
  entryComponents:[
    ModalFormIdefluxComponent
  ],
  exports: [
    MatChipsModule
  ],
  providers: [{
    provide: HIGHLIGHT_OPTIONS,
    useValue: {
      fullLibraryLoader: () => import('highlight.js'),
    }
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
