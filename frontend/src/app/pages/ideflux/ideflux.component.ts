import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ideflux } from 'src/app/models/ideflux.model';
import { IdefluxService } from 'src/app/services/ideflux.service';
import { Maybe } from 'src/app/tools/type';

@Component({
  selector: 'app-ideflux',
  templateUrl: './ideflux.component.html',
  styleUrls: ['./ideflux.component.scss']
})
export class IdefluxComponent implements OnInit {

  idefluxSearch : string
  constructor(
    private router : Router,
    private idefluxService : IdefluxService
  ) { 
    this.idefluxSearch = ""
  }

  ngOnInit(): void {
    this.initData();
  }

  get idefluxs() : Maybe<Ideflux[]> {
    return this.idefluxService.idefluxs
  }

  initData() {
    this.idefluxService.getIdefluxs()
  }

  isDataInit() : boolean {
    return(this.idefluxs ? true : false)
  }

  openIdeflux(ideflux : Ideflux) {
    this.router.navigate(['ideflux', ideflux.id])
  }

}
