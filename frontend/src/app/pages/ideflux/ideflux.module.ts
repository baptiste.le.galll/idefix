import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IdefluxPipe } from 'src/app/pipes/ideflux.pipe';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    IdefluxPipe
  ]
})
export class IdefluxModule { }
