import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Ideflux } from 'src/app/models/ideflux.model';
import { Project } from 'src/app/models/project.model';
import { Tag } from 'src/app/models/tag.model';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';
import { IdefluxService } from 'src/app/services/ideflux.service';
import { ProjectService } from 'src/app/services/project.service';
import { TagService } from 'src/app/services/tag.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  user : User;
  tagSearch : string;
  updateUserForm : FormGroup;
  errorUpdate    : boolean = false;
  successUpdate: boolean = false;
  updateUserTagForm : FormGroup;
  tags : Tag[];
  myTags : Tag[];
  myIdeflux : Ideflux[];
  myProjects : Project[];

  constructor(
    private authService : AuthService,
    private tagService : TagService,
    private idefluxService: IdefluxService,
    private router : Router,
    private projectService : ProjectService
    ) {
      this.tagSearch = "";
      this.myProjects = [];
      this.myIdeflux = [];
      this.myTags = [];   
      this.tags = [];
      this.user = this.authService.user;
      this.updateUserForm = new FormGroup({
        pseudo               : new FormControl(this.user.pseudo, [Validators.required]),
        email                : new FormControl(this.user.email, [Validators.required]),
        password             : new FormControl("",[Validators.required]),
        passwordConfirmation : new FormControl("",[Validators.required]),
      });
      this.updateUserTagForm = new FormGroup({
      
    })
  }

  ngOnInit(): void {
    this.getAllTagByUserId();
    this.getAllTag();
    this.getAllIdefixByUserId()
    this.getAllProjectByUserId()
  }

  getAllIdefixByUserId() : void {
    this.idefluxService.getAllIdefluxByUserId(this.user.id)
    .then((idefluxList : Ideflux[]) => {
      this.myIdeflux = idefluxList;
    })
  }

  getAllProjectByUserId() : void {
    this.projectService.getProjectByUserId(this.user.id)
    .then((projectList : Project[]) => {
      this.myProjects = projectList;
    })
  }

  openIdeflux(ideflux : Ideflux) {
    this.router.navigate(['ideflux', ideflux.id])
  }
  openProject(project : Project) {
    this.router.navigate(['project', project.id])
  }
  getAllTag() : void {
    this.tagService.getTags()
    .then((tagList: Tag[]) => {
      let tagIdList = this.myTags.map(x => x.id);
      this.tags = tagList.filter(tag => !tagIdList.includes(tag.id)) 
    })
  }

  getAllTagByUserId() : Tag[] {
    return this.myTags = this.user.tags;
  }

  addUserTag(tagId : number) : void {
    this.tagService.addUserTag(tagId, this.authService.user.id)
    .then((res : Tag) => {
      this.myTags.push(res);
      this.tags = this.tags.filter(tag => tag.id !== res.id)
      this.authService.updateTagsUser(this.myTags);
    })
  }

  deleteUserTag(tagId : number) : void {
    this.tagService.removeUserTag(tagId, this.authService.user.id)
    .then((tagDeleted : Tag) => {
      this.tags.push(tagDeleted);
      this.myTags = this.myTags.filter(tag => tag.id !== tagDeleted.id)
      this.authService.updateTagsUser(this.myTags);
    })
  }

  updateUser() : any {
    let newUser : User = this.user;
    newUser.pseudo   = this.updateUserForm.value.pseudo;
    newUser.email    = this.updateUserForm.value.email;
    newUser.password = this.updateUserForm.value.password;
    this.authService.updateUser(newUser)
    .then(
      (user : User) => {
        this.user = user;
        this.updateUserForm.value.pseudo = this.user.pseudo
        this.updateUserForm.value.email = this.user.email
        this.updateUserForm.value.password = "";
        this.updateUserForm.value.passwordConfirmation = "";
        this.successUpdate = true;
      }
    )
    .catch(
      (err : any) => {
        this.errorUpdate = true
      }
    )
  }

  

}
