import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagPipe } from 'src/app/pipes/tag.pipe';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TagPipe,
  ]
})
export class AccountModule { }
