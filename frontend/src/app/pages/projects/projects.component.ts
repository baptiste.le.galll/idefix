import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project.model';
import { Tag } from 'src/app/models/tag.model';
import { Router } from '@angular/router';
import { Maybe } from 'src/app/tools/type';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  tags : Maybe<Tag[]>;
  projectSearch : string;

  constructor(
    private router : Router,
    private projectService : ProjectService
  ) { 
    this.projectSearch = ""
  }

  get projects() : Maybe<Project[]> {
    return this.projectService.projects;
  }

  ngOnInit(): void {
    this.initData();
  }

  initData() {
    this.projectService.getProjects()
  }

  openProject(project : Project) {
    this.router.navigate(['project', project.id])
  }

}
