import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectPipe } from 'src/app/pipes/project.pipe';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProjectPipe
  ]
})
export class ProjectsModule { }
