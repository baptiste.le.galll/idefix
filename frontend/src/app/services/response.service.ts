import { Injectable } from '@angular/core';
import { Response } from '../models/response.model';
import { HttpRequestService } from './http-request.service';

@Injectable({
  providedIn: 'root'
})
export class ResponseService {

  response: Response;

  constructor(
    private httpRequestService: HttpRequestService,
  ) {
    this.response = new Response();
  }

  addResponse(content : string, postId: number, createdUserId: number ) : Promise<Response> {
    const responseParams = {
      content : content,
      post_id: postId,
      created_user_id : createdUserId
    };
    return this.httpRequestService.post("responses", responseParams)
    .then((res:any) =>{
      return this.response = Response.asResponse(res);
    })

  }
}
