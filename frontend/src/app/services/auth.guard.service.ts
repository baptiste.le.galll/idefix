
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private authService : AuthService,
    private router : Router
  ) { }

  canActivate(): boolean {
    if (!this.authService.isLogin()) {
      this.router.navigate(['ideflux']);
      return false;
    }
    return true;
  }
}