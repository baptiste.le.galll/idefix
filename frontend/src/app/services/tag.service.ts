import { Injectable } from '@angular/core';
import { Tag } from '../models/tag.model';
import { User } from '../models/user.model';
import { ERROR_TAG_TITLE_TO_TAG_ID } from '../tools/constants';
import { Maybe } from '../tools/type';
import { HttpRequestService } from './http-request.service';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  tags : Tag[];
  
  constructor(
    private httpRequestService : HttpRequestService
  ) { 
    this.tags = [];
  }

  getTags() : Promise<Tag[]> {
    if(this.tags.length !== 0) {
      return Promise.resolve(this.tags);
    } else {
      return this.httpRequestService.get('tags')
      .then(
        (res : any) => {
          return this.tags = Tag.asTags(res)
        }
       )
    }
  }


  async addTag(title: string, description: string) : Promise<Tag> {
    const paramsTag = {
      title : title,
      description : description
    }

    return this.httpRequestService.post("tags", paramsTag)
    .then(
      (res : any) => {
        const tag = Tag.asTag(res);
        this.tags.push(tag);
        return tag
      }
    )
  }


  getTagsIdFromTagsString(tagsTitle : string[]) : number[] {
    return tagsTitle.map(
      (title : string) => {
        const tag : Maybe<Tag> = this.tags.find(
          (tag : Tag) => tag.title === title
        )
        if (tag)
          return tag.id

        return ERROR_TAG_TITLE_TO_TAG_ID
      }
    )
  }

  addUserTag(tagId : number, userId : number) : Promise<Tag> {
    const paramsUserTag = {
      tag_id : tagId,
      user_id : userId
    }
    return this.httpRequestService.post("users/tags", paramsUserTag)
    .then((data : any) => {
      let newTag = Tag.asTag(data);
      return newTag;
    })
  }

  removeUserTag(tagId: number, userId: number) : Promise<Tag> {
    const paramsUserTag = {
      tag_id : tagId,
      user_id : userId
    }
    return this.httpRequestService.post("users/tags/delete", paramsUserTag)
    .then((data : any) => {
      let newTag = Tag.asTag(data);
      return newTag
    })
  }
}
