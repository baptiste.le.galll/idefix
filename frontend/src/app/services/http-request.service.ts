import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_API } from '../tools/constants';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {

  constructor(
    private httpClient : HttpClient
  ) {}

  get(route : string) : Promise<any> {
    return this.httpClient.get(URL_API+route).toPromise()
    .then(
      (res : any) => {
        if (res.status.success) {
          return res.data
        } else {
          return Promise.reject(res.error);
        }
      }
    )
  }

  post(route : string, body : Object) : Promise<any> {
    return this.httpClient.post(URL_API+route, body).toPromise()
    .then(
      (res : any) => {
        if (res.status.success) {
          return res.data
        } else {
          return Promise.reject(res.error);
        }
      }
    );
  }
}