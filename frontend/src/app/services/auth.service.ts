import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Tag } from '../models/tag.model';
import { User } from '../models/user.model';
import { NO_TOKEN } from '../tools/constants';
import { Maybe } from '../tools/type';
import { HttpRequestService } from './http-request.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user:  User;
  token : string;
  
  private isLog : boolean;

  constructor(
    private httpRequestService: HttpRequestService,
    private router : Router
  ) { 
    this.user = this.getLocalStorageUser();
    this.isLog = this.getLocalStorageIsLog();
    this.token = NO_TOKEN;
  }

  isLogin() : boolean {
    return this.isLog;
  }

  disconnect() : void {
    this.user = new User();
    this.isLog = false;
    localStorage.clear();
    this.router.navigate(['/ideflux'])
  }

  getLocalStorageUser() : User {
    let userString : Maybe<string> = localStorage.getItem('currentUser')
    if (userString) {
      return User.asUser(JSON.parse(userString))
    }

    return new User();
  }

  getLocalStorageIsLog() : boolean {
    let isLogString : Maybe<string> = localStorage.getItem('currentIsLog')

    if (isLogString) {
      return isLogString === "true"
    }

    return false
  }


  login(email: string, password: string) : Promise<User> {
    return this.httpRequestService.post("users/login", {email, password})
    .then((res: any) => {
        const user : User = User.asUser(res.user)
        this.token = res.token;
        this.user = user;
        this.isLog = true;
        localStorage.setItem('currentUser', JSON.stringify(user.asJson()))
        localStorage.setItem('currentIsLog', JSON.stringify(this.isLog))
        return user
    })
  }

  signIn(email: string, password: string, pseudo: string, ) : Promise<any> {
    return this.httpRequestService.post("users/sign-in", {email, password, pseudo})
    .then((res: any) => {
      const user : User = User.asUser(res)
      this.user = user;
      return true
    })
    .catch((error:any) => {
      return Promise.reject(new Error(error.message))
    })
  }

  updateUser(newUser : User) {
    return this.httpRequestService.post('users/update/'+this.user.id, newUser.asJson())
    .then(
      (x : any) => {
        this.user = newUser
        localStorage.setItem('currentUser', JSON.stringify(newUser.asJson()))
        return this.user
      }
    )
  }

  updateTagsUser(tags : Tag[]) {
    this.user.tags = tags
    localStorage.setItem('currentUser', JSON.stringify(this.user.asJson()))
  }

 
}
