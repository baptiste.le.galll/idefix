import { Injectable } from '@angular/core';
import { interval } from 'rxjs';
import { Ideflux } from '../models/ideflux.model';
import { Post } from '../models/post.model';
import { Maybe } from '../tools/type';
import { HttpRequestService } from './http-request.service';

@Injectable({
  providedIn: 'root'
})
export class IdefluxService {

  idefluxs      : Maybe<Ideflux[]>;
  currentIdeflux: Maybe<Ideflux>;
  ideflux       : Ideflux;
  constructor(
    private httpRequestService : HttpRequestService
  ) { 

    // interval(50000).subscribe(x => {
    //   this.getIdefluxs();
    // });

    this.ideflux = new Ideflux()
    this.getIdefluxs();
  }


  getIdefluxs() : Promise<Ideflux[]> {
    return this.httpRequestService.get('ideflux')
    .then(
      (res : any) => {
        const  idefluxs      = Ideflux.asIdefluxs(res);
        const  sortedIdeflux = this.sortByDate(idefluxs);
        return this.idefluxs = sortedIdeflux
      }
    )
  }

  async addIdeflux(ideflux : any) : Promise<Ideflux> {
    
    const paramsPost = {
      title : ideflux.post.title,
      content : ideflux.post.content,
      git_link : ideflux.post.gitLink,
      created_user_id : ideflux.post.createdUserId,
    };

    const post = Post.asPost(await this.httpRequestService.post("posts", paramsPost))

    const paramsIdeflux = {
      post_id : post.id,
      tags: ideflux.tags,
    }


    const newIdeflux = Ideflux.asIdeflux(await this.httpRequestService.post("ideflux", paramsIdeflux))
    await this.getIdefluxs()

    return newIdeflux;
  }

  updateIdeflux(ideflux : Ideflux) : Promise<Ideflux> {
    return this.httpRequestService.post("ideflux/update/"+ideflux.id, {ideflux : ideflux.asJson()})
    .then((updatedIdeflux: Ideflux) => {
      return this.ideflux = Ideflux.asIdeflux(updatedIdeflux)
    })
  }

  getIdefluxByPostId(postId: number) : Promise<Ideflux> {
    return this.httpRequestService.get("ideflux/"+postId)
    .then(
      (res : any) => {
        return this.ideflux = Ideflux.asIdeflux(res)
      }
    )
  }

  sortByDate(idefluxs : Ideflux[]) : Ideflux[] {
    return idefluxs.sort((a : Ideflux, b : Ideflux) => (b.id - a.id));
  }

  getAllIdefluxByUserId(userId : number) : Promise<Ideflux[]> {
    return this.httpRequestService.get("ideflux/user/"+userId)
    .then(
      (idefluxList: Ideflux[]) => {
        return this.idefluxs = Ideflux.asIdefluxs(idefluxList)
      }
    )
  }
}

  
