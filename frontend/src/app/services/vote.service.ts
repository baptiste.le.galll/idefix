import { Injectable } from '@angular/core';
import { Vote } from '../models/vote.model';
import { Maybe } from '../tools/type';
import { HttpRequestService } from './http-request.service';

@Injectable({
  providedIn: 'root'
})
export class VoteService {
  vote : Vote
  constructor(
    private httpRequestService : HttpRequestService
  ) { 
    this.vote = new Vote()
  }

  addVote(isUp : boolean, postId : number, responseId: number, createdUserId : number ) : Promise<Vote> {
    const paramsVote = {
      is_up : isUp,
      post_id : postId,
      response_id : responseId,
      created_user_id : createdUserId,
    }
    return this.httpRequestService.post("votes", paramsVote)
      .then((res:any) => {
        return this.vote = Vote.asVote(res)
      })
      .catch((res:any) => {
        return this.vote = Vote.asVote(res.error.error.error.data)
      })
  }

  getVoteByUserId(user_id : number, voteList : Vote[]) : Maybe<Vote> {
    return voteList.find((vote: Vote) => vote.createdUserId === user_id)
  }

  updateVote(vote : Vote, isUp : boolean) {
    vote.isUp = isUp
    return this.httpRequestService.post("votes/update/"+vote.id, vote.asJson())
    .then((res:any) => {
      return this.vote = Vote.asVote(res)
    })
    .catch((res:any) => {
      return this.vote = Vote.asVote(res.error.error.error.data)
    })

  }

  deleteVote(voteByUserId : Vote) : Promise<boolean>{
    return this.httpRequestService.post("votes/delete/"+voteByUserId.id, {})
    .then((res: any) => {
      return res;
    })
  }

  deleteVoteFromList(listVote: Vote[], vote: Vote) : Vote[]{
    let newList: Vote[] = [];
    listVote.forEach( x => {
      if(x.id !== vote.id) {
        newList.push(x);
      }
    })
    return newList;
  }
}
