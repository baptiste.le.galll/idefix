import { Injectable } from '@angular/core';
import { Post } from '../models/post.model';
import { Project } from '../models/project.model';
import { Tag } from '../models/tag.model';
import { Maybe } from '../tools/type';
import { HttpRequestService } from './http-request.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  projects : Maybe<Project[]>;
  project : Maybe<Project>;
  tags: Maybe<Tag[]>;

  constructor(
    private httpRequestService : HttpRequestService
  ) { 
    this.getProjects();
  }

  getProjects() : Promise<Project[]> {
    return this.httpRequestService.get('projects')
    .then(
      (res : any) => {
        const projects = Project.asProjects(res)
        return this.projects = this.sortByDate(projects);
      }
    )
  }

  async addProject(project : any) : Promise<Project> {
    const paramsProject = {
      title : project.title,
      content : project.content,
      git_link : project.gitLink,
      created_user_id : project.createdUserId,
      access_token : project.access_token,
      path_folder : project.path_folder, 
      user_group_id : project.user_group_id,
      tags : project.tags
    };

    const newProject = Project.asProject(await this.httpRequestService.post("projects", paramsProject))
    await this.getProjects()
    return newProject;
  }


  async addPost(newPost : any, project: Project) : Promise<Post> {
    
    const paramsPost = {
      title : newPost.title,
      content : newPost.content,
      git_link : newPost.gitLink,
      created_user_id : newPost.createdUserId,
    };

    const createdPost = Post.asPost(await this.httpRequestService.post("posts", paramsPost))
    
    const paramsProjectPost = {
      project_id: project.id,
      post_id: createdPost.id
    }

    await this.httpRequestService.post("projects/addpost", paramsProjectPost)
    return createdPost;
    
  }

  getProject(id : number) : Promise<Project> {
    return this.httpRequestService.get('projects/'+id)
    .then(
      (res : any) => {
        return this.project = Project.asProject(res)
      }
    )
  }

  getProjectByUserId(userId : number) : Promise<Project[]> {
    return this.httpRequestService.get('projects/user/'+userId)
    .then(
      (projectList : Project[]) => {
        return this.projects = Project.asProjects(projectList)
      }
    )
  }

  getPostFromProject(postId: number) : Promise<Post> {
    return this.httpRequestService.get("posts/"+postId)
    .then(
      (data : any) => {
        return Post.asPost(data)
      }
    )
  }

  sortByDate(projects : Project[]) : Project[] {
    return projects.sort((a : Project, b : Project) => (b.id - a.id));
  }
 
}
