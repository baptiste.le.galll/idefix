import { Injectable } from '@angular/core';
import { Post } from '../models/post.model';
import { Maybe } from '../tools/type';
import { HttpRequestService } from './http-request.service';
import { IdefluxService } from './ideflux.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  post: Maybe<Post>;
  posts : Maybe<Post[]>;
  constructor(
    private httpRequestService: HttpRequestService,
    private idefluxService    : IdefluxService
  ) {}

}
